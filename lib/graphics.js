/**
 * @ts-check
 * @overview Library of game graphics references (simple emoji codes) and text fallbacks
 * @module   graphics
 * @requires assert
 */

'use strict';

import assert from 'assert';

const ERROR_UKNOWN_GRAPHIC = 'Unknown graphic specified for `Graphics#render`';

/**
 * All known graphics for the game
 * @type {Map.<string, Object>}
 */
const GRAPHICS = new Map()
  .set('logo', { sprite: ':battlebate_logo', icon: ':battlebate_logo', text: null })
  .set('bullet', { sprite: ':battlebate_sword', icon: ':battlebate_sword', text: '*' })
  .set('item', { sprite: ':battlebate_item', icon: ':battlebate_item', text: null })
  .set('treasure', { sprite: ':battlebate_treasure', icon: ':battlebate_treasure', text: null })
  .set('crit', { sprite: ':battlebate_crit', icon: ':battlebate_crit', text: '***' })
  .set('level-up', { sprite: ':battlebate_level_up', icon: ':battlebate_level_up', text: null })
  .set('ghost', { sprite: ':battlebate_ghost', icon: null, text: null })
  .set('skeleton-swordsman', { sprite: ':battlebate_skeleton_swordsman', icon: null, text: null })
  .set('skeleton-archer', { sprite: ':battlebate_skeleton_archer', icon: null, text: null })
  .set('lizardman', { sprite: ':battlebate_lizardman', icon: null, text: null })
  .set('fire-elemental', { sprite: ':battlebate_fire_elemental', icon: null, text: null })
  .set('half-dragon', { sprite: ':battlebate_half_dragon', icon: null, text: null })
  .set('myrmidon', { sprite: ':battlebate_myrmidon', icon: null, text: null })
  .set('ogre', { sprite: ':battlebate_ogre', icon: null, text: null })
  .set('orc-chieftain', { sprite: ':battlebate_orc_chieftain', icon: null, text: null })
  .set('troll', { sprite: ':battlebate_troll', icon: null, text: null })
  .set('wraith', { sprite: ':battlebate_wraith', icon: null, text: null })
  .set('ancient-dragon', { sprite: ':battlebate_ancient_dragon', icon: null, text: null })
  .set('awakened-tree', { sprite: ':battlebate_awakened_tree', icon: null, text: null })
  .set('draugr', { sprite: ':battlebate_draugr', icon: null, text: null })
  .set('lich', { sprite: ':battlebate_lich', icon: null, text: null })
  .set('undead-dragon', { sprite: ':battlebate_undead_dragon', icon: null, text: null })
  .set('bard-novice', { sprite: ':battlebate_bard_novice', icon: ':battlebate_bard_icon', text: null })
  .set('bard-elite', { sprite: ':battlebate_bard_elite', icon: ':battlebate_bard_icon', text: null })
  .set('bard-legendary', { sprite: ':battlebate_bard_legendary', icon: ':battlebate_bard_icon', text: null })
  .set('fighter-novice', { sprite: ':battlebate_fighter_novice', icon: ':battlebate_fighter_icon', text: null })
  .set('fighter-elite', { sprite: ':battlebate_fighter_elite', icon: ':battlebate_fighter_icon', text: null })
  .set('fighter-legendary', { sprite: ':battlebate_fighter_legendary', icon: ':battlebate_fighter_icon', text: null })
  .set('ranger-novice', { sprite: ':battlebate_ranger_novice', icon: ':battlebate_ranger_icon', text: null })
  .set('ranger-elite', { sprite: ':battlebate_ranger_elite', icon: ':battlebate_ranger_icon', text: null })
  .set('ranger-legendary', { sprite: ':battlebate_ranger_legendary', icon: ':battlebate_ranger_icon', text: null })
  .set('warlock-novice', { sprite: ':battlebate_warlock_novice', icon: ':battlebate_warlock_icon', text: null })
  .set('warlock-elite', { sprite: ':battlebate_warlock_elite', icon: ':battlebate_warlock_icon', text: null })
  .set('warlock-legendary', { sprite: ':battlebate_warlock_legendary', icon: ':battlebate_warlock_icon', text: null });

class Graphics {
  /**
   * @param {Object} cb - Chaturbate interface
   */
  constructor(cb) {
    /** @type {Object} */
    this.cb = cb;

    /** @type {Map.<string, Object>} */
    this.library = GRAPHICS;
  }

  /**
   * Renders a given graphic
   * @param   {Object} name - Name of the graphic to "render"
   * @returns {string}
   */
  render(name) {
    assert(GRAPHICS.has(name), `${ERROR_UKNOWN_GRAPHIC}: "${name}"`);

    const graphic = GRAPHICS.get(name);

    return graphic[this.cb.settings.graphics] || '';
  }
}

export {
  ERROR_UKNOWN_GRAPHIC
};

export default Graphics;
