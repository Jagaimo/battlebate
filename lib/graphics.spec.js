/* eslint-env mocha */

'use strict';

import { AssertionError } from 'assert';
import chai from 'chai';

import {
  default as Graphics,
  ERROR_UKNOWN_GRAPHIC
} from './graphics';

function graphics(cb) {
  return new Graphics(cb);
}

const should = chai.should();
const graphicsText = graphics({ settings: { graphics: 'text' } });
const graphicsIcon = graphics({ settings: { graphics: 'icon' } });
const graphicsSprite = graphics({ settings: { graphics: 'sprite' } });

describe('Graphics', () => {
  describe('#render', () => {
    it('should throw an error if the specified graphic is unknown', () => (
      graphicsText.render.bind(graphicsText, '<unknown>').should.throw(AssertionError, `${ERROR_UKNOWN_GRAPHIC}: "<unknown>"`)
    ));

    context('when graphics mode is "text"', () => {
      it('should return each graphic correctly', () => (
        graphicsText.library.forEach((props, name) => {
          const rendering = graphicsText.render(name);

          if (props.text) {
            should.equal(rendering, props.text);
          } else {
            should.equal(rendering, '');
          }
        })
      ));
    });

    context('when graphics mode is "icon"', () => {
      it('should return each graphic correctly', () => (
        graphicsIcon.library.forEach((props, name) => {
          const rendering = graphicsIcon.render(name);

          if (props.icon) {
            should.equal(rendering, props.icon);
          } else {
            should.equal(rendering, '');
          }
        })
      ));
    });

    context('when graphics mode is "sprite"', () => {
      it('should return each graphic correctly', () => (
        graphicsSprite.library.forEach((props, name) => {
          const rendering = graphicsSprite.render(name);

          if (props.sprite) {
            should.equal(rendering, props.sprite);
          } else {
            should.equal(rendering, '');
          }
        })
      ));
    });
  });
});
