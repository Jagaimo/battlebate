/* eslint-env mocha */

'use strict';

import Game from './game';

function trim(string) {
  return string.trim().replace(/\s+/g, ' ');
}

describe('Game', () => {
  let cb;
  let game;

  beforeEach(() => {
    cb = {
      _callbacks: {
        onTip: null,
        onMessage: null,
        onEnter: null,
        onDrawPanel: null,
        tipOptions: null
      },

      tip(item) {
        this._callbacks.onTip.call(null, item);
      },

      message(item) {
        return this._callbacks.onMessage.call(null, item);
      },

      enter(entrance) {
        this._callbacks.onEnter.call(null, entrance);
      },

      drawPanel() {
        this._callbacks.onDrawPanel.call(null);
      },

      onTip(callback) {
        this._callbacks.onTip = callback;
      },

      onMessage(callback) {
        this._callbacks.onMessage = callback;
      },

      onEnter(callback) {
        this._callbacks.onEnter = callback;
      },

      onDrawPanel(callback) {
        this._callbacks.onDrawPanel = callback;
      },

      changeRoomSubject: global.sandbox.stub(),

      sendNotice: global.sandbox.stub(),

      settings: {
        level_up_requirement: 250,
        legendary_rank_xp: 1000,
        elite_rank_xp: 500,
        goal: 1000,
        loot_1: '<loot.1>',
        loot_2: '<loot.2>',
        loot_3: '<loot.3>',
        bardic_song_effect: '<bardic.song.effect>',
        command_effect: '<command.effect>',
        called_shot_target: '<called.shot.effect>',
        graphics: 'sprite'
      },

      room_slug: '<broadcaster>'
    };

    game = new Game(cb).start();

    global.sandbox.spy(game.messenger, 'whisper');
    global.sandbox.spy(game.messenger, 'say');
    global.sandbox.spy(game.messenger, 'announce');
    global.sandbox.spy(game.messenger, 'taunt');
  });

  describe('#addPlayer', () => {
    let player;

    beforeEach(() => {
      player = '<user>';
      game.addPlayer(player);
    });

    it('should add the given player to the party', () => (
      game.players.has(player).should.be.true
    ));
  });

  describe('#setupPrizes', () => {
    it('should assign prizes', () => (
      game.prizes.should.eql({
        bardicSong: '<bardic.song.effect>',
        command: '<command.effect>',
        calledShot: '<called.shot.effect>',
        loot: [
          '<loot.1>',
          '<loot.2>',
          '<loot.3>'
        ]
      })
    ));
  });

  describe('#spawnEnemy', () => {
    let previousEnemy;

    beforeEach(() => {
      previousEnemy = game.enemy;
      game.spawnEnemy(500);
    });

    it('should replace the current enemy instance', () => (
      game.enemy.should.not.equal(previousEnemy)
    ));

    it('should announce the new enemy', () => (
      game.messenger.taunt.should.have.been.calledWith(`${game.graphics.render(game.enemy.graphic)}...suddenly, a vile ${game.enemy.name} appears! Dare you challenge its might?`)
    ));

    it('should assign a hitpoints value', () => (
      game.enemy.hp.should.equal(cb.settings.goal)
    ));
  });

  describe('#logReward', () => {
    let reward;

    beforeEach(() => {
      reward = { label: '<label>', user: '<user>', time: new Date() };
      game.rewardLog.push(reward);
    });

    it('should log a given reward', () => {
      game.logReward(reward);
      game.rewardLog.should.include(reward);
    });
  });

  context('when the game starts', () => {
    let clock;

    beforeEach(() => {
      clock = global.sandbox.useFakeTimers();
      game = new Game(cb).start();

      global.sandbox.spy(game.messenger, 'announce');
    });

    it('should set a reminder to let players know they have unused items', () => {
      cb.enter({ user: '<user.1>' });

      game.players.get('<user.1>').inventory.push('<item>');
      clock.tick(1800000);

      const announce = game.messenger.announce.getCall(2);

      trim(announce.args[0]).should.equal(trim(`
        Your inventory: <item> (x1)
        ::: Type /use item_name to expend an item (e.g. /use <item>) :::
      `));

      announce.args[1].should.equal('<user.1>');
    });

    it('should set a reminder to broadcast the game is running', () => {
      clock.tick(900000);

      const announce = game.messenger.announce.getCall(0);

      trim(announce.args[0]).should.equal(trim(`
        This room is running Battlebate, the Chaturbate fantasy role-playing game.
        Type /help to learn more about it.
      `));

      announce.args.should.have.length(1);
    });
  });

  context('when a tip is received', () => {
    it('should trigger an attack from the player', () => {
      const entrance = { user: '<user>' };
      const tip      = { from_user: '<user>', amount: 5, message: '' };
      let attack;

      cb.enter(entrance);
      attack = global.sandbox.spy(game.players.get('<user>'), 'attack');

      cb.tip(tip);
      attack.should.have.been.calledWith(game.enemy, 5);
    });

    context('when damage is enough to kill an enemy', () => {
      it('should spawn a new enemy', () => {
        const attackTip = { from_user: '<user>', amount: 1000, message: '' };

        cb.enter({ user: '<user>' });
        cb.tip(attackTip);

        game.enemy.hp.should.equal(1000);
      });
    });

    context('when damage is overkill', () => {
      it('should carry over to the next enemy', () => {
        const attackTip = { from_user: '<user>', amount: 1500, message: '' };

        cb.enter({ user: '<user>' });
        cb.tip(attackTip);

        game.enemy.hp.should.equal(500);
      });

      it('should carry over across an enemy life', () => {
        const attackTip = { from_user: '<user>', amount: 1500, message: '' };

        cb.enter({ user: '<user>' });
        cb.tip(attackTip);

        game.totalKills.should.equal(1);
        game.enemy.hp.should.equal(500);
      });

      it('should carry over across multiple enemy lives', () => {
        const attackTip = { from_user: '<user>', amount: 3250, message: '' };

        cb.enter({ user: '<user>' });
        cb.tip(attackTip);

        game.totalKills.should.equal(3);
        game.enemy.hp.should.equal(750);
      });

      context('when the player lands a critical hit', () => {
        it('should announce the effect', () => {
          cb.enter({ user: '<user>' });

          const attackTip = { from_user: '<user>', amount: 500, message: '' };
          const player    = game.players.get('<user>');

          player.changeClass('fighter');
          global.sandbox.stub(player, 'attack').returns({ rolls: [20] });
          cb.tip(attackTip);

          game.messenger.announce.should.have.been.calledWithMatch(player.critText());
        });
      });
    });

    context('when the player has no class', () => {
      it('should message the player about selecting a class', () => {
        const attackTip = { from_user: '<user>', amount: 100, message: '' };
        const showClasses = global.sandbox.spy(game.info, 'showClasses');

        cb.enter({ user: '<user>' });
        cb.tip(attackTip);

        showClasses.should.have.been.calledWith(game.players.get('<user>'));
      });
    });
  });

  context('when the player levels up', () => {
    beforeEach(() => {
      const entrance = { user: '<user>' };
      const tip      = { from_user: '<user>', amount: 250, message: '' };

      cb.enter(entrance);
      cb.tip(tip);
    });

    it('should send a notice about the level up', () => {
      game.messenger.announce.should.have.been.calledWithMatch(/<user> has reached Level 2!/);
    });

    it('should give the player loot', () => {
      game.players.get('<user>').inventory.should.not.be.empty;
    });
  });

  context('when a message is received', () => {
    beforeEach(() => {
      global.sandbox.spy(cb._callbacks, 'onMessage');
      cb.message({ user: '<user>', m: '<message>' });
    });

    it('should fire the `onMessage` callback', () => {
      cb._callbacks.onMessage.should.have.been.called;
    });
  });

  context('when a message is received that is not a command', () => {
    let message;

    context('when the graphics are set to `text` and the player has no class', () => {
      beforeEach(() => {
        cb.settings.graphics = 'text';
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':: Lv. 1 Adventurer :: <message>',
          user: '<user>'
        })
      ));
    });

    context('when the graphics are set to `text` and the player has chosen a class', () => {
      beforeEach(() => {
        cb.settings.graphics = 'text';
        cb.message({ user: '<user>', m: '/class ranger' });
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':: Lv. 1 Ranger :: <message>',
          user: '<user>'
        })
      ));
    });

    context('when the graphics are set to `icon` and the player has no class', () => {
      beforeEach(() => {
        cb.settings.graphics = 'icon';
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':: Lv. 1 :: <message>',
          user: '<user>'
        })
      ));
    });

    context('when the graphics are set to `icon` and the player has chosen a class', () => {
      beforeEach(() => {
        cb.settings.graphics = 'icon';
        cb.message({ user: '<user>', m: '/class ranger' });
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':: Lv. 1 :battlebate_ranger_icon :: <message>',
          user: '<user>'
        })
      ));
    });

    context('when the graphics are set to `sprite` and the player has no class', () => {
      beforeEach(() => {
        cb.settings.graphics = 'sprite';
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':: Lv. 1 :: <message>',
          user: '<user>'
        })
      ));
    });

    context('when the graphics are set to `sprite` and the player has chosen a class', () => {
      beforeEach(() => {
        game.graphics.displayMode = 'sprite';
        cb.message({ user: '<user>', m: '/class ranger' });
        message = cb.message({ user: '<user>', m: '<message>' });
      });

      it('should decorate the message', () => (
        message.should.eql({
          m: ':battlebate_ranger_novice :: <message>',
          user: '<user>'
        })
      ));
    });
  });

  context('when a message is received for changing class', () => {
    let changeClass;

    beforeEach(() => {
      const entrance = { user: '<user>' };

      cb.enter(entrance);
      changeClass = global.sandbox.spy(game.players.get('<user>'), 'changeClass');
    });

    it('should change the player\'s class', () => {
      cb.message({ user: '<user>', m: '/class fighter' });
      changeClass.should.have.been.calledWith('fighter');
    });

    context('when the class is specified is invalid', () => {
      it('should show a notice', () => {
        cb.message({ user: '<user>', m: '/class <null>' });
        game.messenger.whisper.should.have.been.calledWith('Unknown hero class selected for /class command. Valid options are: fighter, ranger, warlock, bard.', '<user>');
      });
    });
  });

  context('when a message is received for using an item', () => {
    beforeEach(() => {
      const entrance = { user: '<user>' };

      cb.enter(entrance);
      game.players.get('<user>').inventory = ['<loot.1>'];
    });

    context('when the player has the item', () => {
      it('should show a notice', () => {
        cb.message({ user: '<user>', m: '/use <loot.1>' });

        const announce = game.messenger.announce.getCall(1);

        trim(announce.args[0]).should.equal(trim(`
          :battlebate_item <user> uses an item: "<loot.1>"! :battlebate_item
        `));

        announce.args.should.have.length(1);
      });

      it('should expend the item', () => {
        const useItem = global.sandbox.spy(game.players.get('<user>'), 'useItem');

        cb.message({ user: '<user>', m: '/use <loot.1>' });
        useItem.should.have.been.calledWith('<loot.1>');
      });

      it('should log the usage', () => {
        const logReward = global.sandbox.spy(game, 'logReward');

        cb.message({ user: '<user>', m: '/use <loot.1>' });
        logReward.should.have.been.calledWith('<user>', '<loot.1>');
      });
    });

    context('when the player does not have the item', () => {
      it('should show a notice', () => {
        cb.message({ user: '<user>', m: '/use <null>' });
        game.messenger.whisper.should.have.been.calledWith(`
          Looks like you don't have that item in your inventory...
          Current inventory: <loot.1> (x1)
        `);
      });
    });
  });

  context('when a message is received to show the guide to the user', () => {
    let player;
    let help;

    beforeEach(() => {
      help = global.sandbox.spy(game.info, 'showHelp');
      cb.message({ user: '<user>', m: '/help' });

      player = game.players.get('<user>');
    });

    it('should show guide', () => {
      help.should.have.been.calledWith(player);
    });
  });

  context('when a message is received to show the guide to everyone', () => {
    let showWelcome;

    beforeEach(() => {
      showWelcome = global.sandbox.spy(game.info, 'showWelcome');
    });

    context('when the user is not authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/help all', is_mod: false });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Only the broadcaster or mods can do that', '<user>')
      ));

      it('should not show the guide', () => (
        showWelcome.should.not.have.been.called
      ));
    });

    context('when the user is a mod', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/help all', is_mod: true });
      });

      it('should show class info', () => (
        showWelcome.should.have.been.called
      ));
    });

    context('when the user is the broadcaster', () => {
      beforeEach(() => {
        cb.message({ user: '<broadcaster>', m: '/help all', is_mod: false });
      });

      it('should show class info', () => (
        showWelcome.should.have.been.called
      ));
    });
  });

  context('when a message is received to show player info', () => {
    let showPlayerInfo;
    let player;

    beforeEach(() => {
      showPlayerInfo = global.sandbox.spy(game.info, 'showPlayerInfo');
      cb.message({ user: '<user>', m: '/info' });

      player = game.players.get('<user>');
    });

    it('should show guide', () => (
      showPlayerInfo.should.have.been.calledWith(player)
    ));
  });

  context('when a message is received to show class info to a user', () => {
    let showClasses;
    let player;

    beforeEach(() => {
      showClasses = global.sandbox.spy(game.info, 'showClasses');
      cb.message({ user: '<user>', m: '/classes', is_mod: false });

      player = game.players.get('<user>');
    });

    it('should show class info', () => (
      showClasses.should.have.been.calledWithExactly(player)
    ));
  });

  context('when a message is received to show class info to everyone', () => {
    let showClasses;

    beforeEach(() => {
      showClasses = global.sandbox.spy(game.info, 'showClasses');
    });

    context('when the user is not authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/classes all', is_mod: false });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Only the broadcaster or mods can do that', '<user>')
      ));

      it('should not show class info', () => (
        showClasses.should.not.have.been.called
      ));
    });

    context('when the user is a mod', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/classes all', is_mod: true });
      });

      it('should show class info', () => (
        showClasses.should.have.been.called
      ));
    });

    context('when the user is the broadcaster', () => {
      beforeEach(() => {
        cb.message({ user: '<broadcaster>', m: '/classes all', is_mod: false });
      });

      it('should show class info', () => (
        showClasses.should.have.been.called
      ));
    });
  });

  context('when a message is received to change the graphics level', () => {
    context('when the user is a mod', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/graphics text', is_mod: true });
      });

      it('should change the graphics level', () => (
        cb.settings.graphics.should.equal('text')
      ));
    });

    context('when the user is the broadcaster', () => {
      beforeEach(() => {
        cb.message({ user: '<broadcaster>', m: '/graphics text', is_mod: false });
      });

      it('should change the graphics level', () => (
        cb.settings.graphics.should.equal('text')
      ));
    });

    context('when the user is not authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/graphics text', is_mod: false });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Only the broadcaster or mods can do that', '<user>')
      ));

      it('should not change the graphics level', () => (
        cb.settings.graphics.should.not.equal('text')
      ));
    });

    context('when a missing or invalid argument is provided', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/graphics', is_mod: true });
      });

      it('should notify the user', () => (
        game.messenger.whisper.should.have.been.calledWith('You dun goofed. Try /graphics sprite|icon|text')
      ));

      it('should not change the graphics level', () => {
        cb.settings.graphics.should.not.equal('text')
      });
    });
  });

  context('when a message is received to show recent prize winners and used items', () => {
    let showRewards;

    beforeEach(() => {
      showRewards = global.sandbox.spy(game.info, 'showRewards');
    });

    context('when the user is authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/rewards', is_mod: true });
        cb.message({ user: '<broadcaster>', m: '/rewards', is_mod: false });
      });

      it('should show prize winners', () => (
        showRewards.should.have.been.calledTwice
      ));
    });

    context('when the user is not authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/rewards', is_mod: false });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Only the broadcaster or mods can do that', '<user>')
      ));

      it('should not show winners', () => (
        showRewards.should.not.have.been.called
      ));
    });
  });

  context('when a message is received to set the goal', () => {
    let spawnEnemy;

    beforeEach(() => {
      spawnEnemy = global.sandbox.spy(game, 'spawnEnemy');
    });

    context('when the token amount is valid', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/goal 1000', is_mod: true });
      });

      it('should spawn a new enemy', () => (
        spawnEnemy.should.have.been.called
      ));
    });

    context('when the token amount is invalid', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/goal 0', is_mod: true });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Choose a token amount greater than 0 (e.g. /goal 1000)', '<user>')
      ));

      it('should not spawn an enemy', () => (
        spawnEnemy.should.not.have.been.called
      ));
    });

    context('when the user is not a authorized', () => {
      beforeEach(() => {
        cb.message({ user: '<user>', m: '/goal 0', is_mod: false });
      });

      it('should notify the user of an error', () => (
        game.messenger.whisper.should.have.been.calledWith('Only the broadcaster or mods can do that', '<user>')
      ));

      it('should not have spawned an enemy', () => (
        spawnEnemy.should.not.have.been.called
      ));
    });
  });
});

