/**
 * @overview Game party member
 * @module   player
 * @requires d20
 */

'use strict';

import d20 from 'd20';

/**
 * Special ability templates
 * @private
 * @type {Map.<Object>}
 */
const ABILITIES = new Map()
  .set('called-shot', {
    name: 'Called Shot',
    type: 'preset',
    effect: null
  })
  .set('bardic-song', {
    name: 'Bardic Song',
    type: 'preset',
    effect: null
  })
  .set('command',  {
    name: 'Command',
    type: 'preset',
    effect: null
  })
  .set('battlecry', {
    name: 'Battlecry',
    type: 'random',
    effect: null
  });

/**
 * Character class properties
 * @private
 * @type {Map.<Object>}
 */
const CLASSES = new Map();

CLASSES.set('bard', {
  ability: ABILITIES.get('bardic-song'),
  graphics: new Map()
    .set('novice', 'bard-novice')
    .set('elite', 'bard-elite')
    .set('legendary', 'bard-legendary'),
  damageText: (player, enemy, damage) => {
    if (damage < 15) {
      return `${player.name} thrusts their rapier at the ${enemy.name}.`;
    }

    if (damage < 35) {
      return `${player.name} confounds the ${enemy.name} with illusion magic!`;
    }

    if (damage < 70) {
      return `${player.name} becomes invisible through magic, ambushing the ${enemy.name} with a series of sword thrusts!`;
    }

    if (damage < 200) {
      return `${player.name} uses empathy magic on ${enemy.name}, exploiting their lowered defenses to deadly effect!`;
    }

    return `
      ${player.name} shouts a magical word of power! Its mind in tatters, the ${enemy.name} forgets who and where they are.
    `.trim();
  },
  critText: player => (
    `${player.name} uses Bardic Song! Everyone is inspired by their performance, *especially* the model...`
  )
});

CLASSES.set('fighter', {
  ability: ABILITIES.get('battlecry'),
  graphics: new Map()
    .set('novice', 'fighter-novice')
    .set('elite', 'fighter-elite')
    .set('legendary', 'fighter-legendary'),
  damageText: (player, enemy, damage) => {
    if (damage < 15) {
      return `${player.name} attacks the ${enemy.name} with their sword.`;
    }

    if (damage < 35) {
      return `${player.name}'s sword bites deep into the ${enemy.name}!`;
    }

    if (damage < 70) {
      return `${player.name} vaults over the ${enemy.name}, then follows up with brutal sword swings!`;
    }

    if (damage < 200) {
      return `${player.name} deflects a blow from the ${enemy.name}, then counter-attacks!`;
    }

    return `
      ${player.name} years into the ${enemy.name} with incredible speed and power!
    `.trim();
  },
  critText: player => (`
    ${player.name} shouts a battlecry to inspire the group!
    The model is given courage and aids the party the best way they know how to, if you catch my drift...
  `)
});

CLASSES.set('ranger', {
  ability: ABILITIES.get('called-shot'),
  graphics: new Map()
    .set('novice', 'ranger-novice')
    .set('elite', 'ranger-elite')
    .set('legendary', 'ranger-legendary'),
  damageText: (player, enemy, damage) => {
    if (damage < 15) {
      return `${player.name} strikes the ${enemy.name} with an arrow from their bow.`;
    }

    if (damage < 35) {
      return `An arrow from ${player.name} penetrates the ${enemy.name}'s defenses!`;
    }

    if (damage < 70) {
      return `${player.name} charges an arrow with electric energy and fires at the ${enemy.name}!`;
    }

    if (damage < 200) {
      return `${player.name} unleashes a magical hailstorm of arrows at the ${enemy.name}!`;
    }

    return `
      ${player.name} summons elementals to fight alongside them, driving the ${enemy.name} back as one!
    `.trim();
  },
  critText: player => (`
    ${player.name}'s arrows land with unerring accuracy!
    The model is temporarily freed, but unintentionally left exposed...*very* exposed...
  `)
});

CLASSES.set('warlock', {
  ability: ABILITIES.get('command'),
  graphics: new Map()
    .set('novice', 'warlock-novice')
    .set('elite', 'warlock-elite')
    .set('legendary', 'warlock-legendary'),
  damageText: (player, enemy, damage) => {
    // 1 - 14
    if (damage < 15) {
      return `${player.name} disrupts the ${enemy.name} with a blast of eldritch energy.`;
    }

    // 15 - 34
    if (damage < 35) {
      return `${player.name} blasts the ${enemy.name} back with telekinetic force!`;
    }

    // 35 - 69
    if (damage < 70) {
      return `${player.name} summons dark magical tendrils to twist and slash at the ${enemy.name}!`;
    }

    // 70 - 199
    if (damage < 200) {
      return `Hellfire bursts forth from ${player.name} to sear the ${enemy.name} with unnatural flames!`;
    }

    // 200+
    return `${player.name} banishes the ${enemy.name} to a hellish plane, where its very soul is violated by demons!`;
  },
  critText: player => (`
    ${player.name} casts a mind control spell!
    ...unfortunately, the model is also brought under their dark control...huehuehuehue...
  `)
});

/**
 * Amount of experience points required to level up
 * @private
 * @type {number}
 */
let LEVEL_UP_REQUIREMENT = 250;

/**
 * Amount of experience points required to pass legendary rank
 * @private
 * @type {number}
 */
let LEGENDARY_RANK_XP = 1000;

/**
 * Amount of experience points required to pass elit rank
 * @private
 * @type {number}
 */
let ELITE_RANK_XP = 500;

/**
 * @class Player
 */
class Player {
  /**
   * Assign custom special effects for a given ability
   * @param   {string}         ability - Name of the ability to change
   * @param   {string} effect - Special effect to apply
   * @returns {undefined}
   */
  static setupAbility(ability, effect) {
    const abilityTemplate = ABILITIES.get(ability);

    if (ability === 'bardic-song') {
      CLASSES.get('bard').ability = Object.assign({}, abilityTemplate, {
        effect
      });
    }

    if (ability === 'command') {
      CLASSES.get('warlock').ability = Object.assign({}, abilityTemplate, {
        effect
      });
    }

    if (ability === 'called-shot') {
      CLASSES.get('ranger').ability = Object.assign({}, abilityTemplate, {
        effect
      });
    }
  }

  /**
   * Set requisite amount of xp for leveling
   * @param   {number} xp - Required amount of xp
   * @returns {undefined}
   */
  static setLevelUpRequirement(xp) {
    LEVEL_UP_REQUIREMENT = xp;
  }

  /**
   * Set requisite amount of xp for pass ranks
   * @param   {number} xp - Required amount of xp
   * @returns {undefined}
   */
  static setRanks(legendaryRankXp, eliteRankXp) {
    LEGENDARY_RANK_XP = legendaryRankXp;
    ELITE_RANK_XP = eliteRankXp;
  }

  static printItems(items) {
    const itemCounts = new Map();
    const itemLabels = [];
    let itemText     = items.length ? '' : '(nothing)';

    // Build a list of item counts
    items.forEach(item => {
      if (!itemCounts.has(item)) {
        itemCounts.set(item, 1);
      } else {
        itemCounts.set(item, itemCounts.get(item) + 1);
      }
    });

    // Build a set of item labels (e.g. ['Some Item (x1)', 'Some Other item (x2)'])
    itemCounts.forEach((count, item) => {
      itemLabels.push(`${item} (x${count})`);
    });

    if (itemLabels.length) {
      itemText = itemLabels.join(', ');
    }

    return itemText;
  }

  /**
   * @constructs Player
   * @param {string} name - Username of the player
   */
  constructor(name) {
    /** @type {string} */
    this.name = name;

    /** @type {number} */
    this.xp = 0;

    /** @type {Array.<string>} */
    this.inventory = [];
  }

  /**
   * Renders an emoji/avatar for the player
   * @readonly
   */
  get emoji() {
    if (!this.characterClass) {
      return null;
    } else if (this.xp >= LEGENDARY_RANK_XP) {
      return this.graphics.get('legendary');
    } else if (this.xp >= ELITE_RANK_XP) {
      return this.graphics.get('elite');
    }

    return this.graphics.get('novice');
  }

  /**
   * Calculates player level based on current XP and the level up requirement
   * @readonly
   */
  get level() {
    return Math.ceil((this.xp + 1) / LEVEL_UP_REQUIREMENT);
  }

  /**
   * Change the character class of the player
   * @param   {string}  selectedClass - Selected character class
   * @returns {Object} `Player` instance
   */
  changeClass(selectedClass) {
    const characterClass = CLASSES.get(selectedClass);

    this.characterClass = selectedClass;
    this.ability = characterClass.ability;
    this.damageText = characterClass.damageText.bind(null, this);
    this.critText = characterClass.critText.bind(null, this);
    this.graphics = characterClass.graphics;

    return this;
  }

  /**
   * Adds a set of items to player's inventory
   * @param {Array.<string>} items - Set of items to add to inventory
   */
  addItems(items) {
    items.forEach(item => this.inventory.push(item));
  }

  /**
   * Expend a given item, removing it from inventory
   * @param   {string} item - Identifier of the item to use
   * @returns {Object} `Player` instance
   */
  useItem(item) {
    let index;

    for (const i of this.inventory) {
      if (i.toLowerCase() === item.toLowerCase()) {
        index = this.inventory.indexOf(i);
        break;
      }
    }

    this.inventory.splice(index, 1);

    return this;
  }

  /**
   * Attack a given enemy
   * @param   {Enemy}  enemy - The enemy to attack
   * @param   {number} damage - Damage to apply (token)
   * @returns {Object} Attack result
   */
  attack(enemy, damage) {
    const result = { rolls: [] };
    let rollsRemaining = damage >= 25 ? Math.floor(damage / 25) : 0;

    // Roll for every 25 tokens
    while (rollsRemaining) {
      result.rolls.push(d20.roll(20));
      rollsRemaining--;
    }

    // Apply damage
    enemy.damage(damage);

    // Acrue experience points
    this.xp += damage;

    return result;
  }
}

export default Player;
