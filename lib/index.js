'use strict';

import 'babel-polyfill';
import Game from './game';

const game = new Game(global.cb);
game.start();
