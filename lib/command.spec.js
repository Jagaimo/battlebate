/* eslint-env mocha */

'use strict';

import Command from './command';

const cb = {
  onMessage: global.sandbox.stub()
};

function build() {
  return new Command(cb);
}

describe('Command', () => {
  let command;
  let callback;
  let message;
  let cmd;

  beforeEach(() => {
    command = build();
    callback = global.sandbox.stub();
  });

  describe('initialize', () => {
    it('should setup an empty `Map` registry of commands', () => (
      command.registry.should.be.a('Map')
    ));
  });

  describe('#register', () => {
    beforeEach(() => {
      command.register('/test', null, callback);
    });

    it('should add the specified command to the registry', () => (
      command.registry.get('/test').should.eql({
        pattern: '/test',
        params: null,
        callback
      })
    ));
  });

  describe('#evaluate', () => {
    context('when no parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', null, callback);

        message = { m: '/test' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, null)
      ));
    });

    context('when a [directive] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'directive', values: ['value'] }], callback);

        message = { m: '/test value' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument: 'value' })
      ));
    });

    context('when a [string] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'string', values: [] }], callback);

        message = { m: '/test "<value>"' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument: '<value>' })
      ));
    });

    context('when an [assignment] parameter is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'assignment', values: [] }], callback);

        message = { m: '/test argument=value' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument: 'value' })
      ));
    });

    context('when multiple parameters are defined', () => {
      beforeEach(() => {
        command.register('/test', [
          { name: 'argument1', type: 'string', values: ['value1'] },
          { name: 'argument2', type: 'assignment', values: ['value2'] }
        ], callback);

        message = { m: '/test "value1" argument=value2' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument1: 'value1', argument2: 'value2' })
      ));
    });

    context('when a command is executed with special characters', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'any', values: [] }], callback);

        message = { m: '/test +1 /foo <value>' };
        cmd = command.registry.get('/test');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument: '+1 /foo <value>' })
      ));
    });

    context('when a command that partially matches another is defined', () => {
      beforeEach(() => {
        command.register('/test', [{ name: 'argument', type: 'any', values: [] }], callback);
        command.register('/tests', [{ name: 'argument', type: 'any', values: [] }], callback);

        message = { m: '/tests <value>' };
        cmd = command.registry.get('/tests');

        command.evaluate(message);
      });

      it('should execute the callback', () => (
        callback.should.have.been.calledWith(message, cmd, { argument: '<value>' })
      ));
    });
  });
});
