/**
 * @ts-check
 * @overview Main game module
 * @module   game
 * @requires command
 * @requires enemy
 * @requires graphics
 * @requires info
 * @requires lodash.sample
 * @requires lodash.startcase
 * @requires messenger
 * @requires player
 */

'use strict';

import sample     from 'lodash.sample';
import startCase from 'lodash.startcase';
import Joi from 'joi';
import { version as VERSION } from '../package.json';

import Command   from './command';
import Enemy     from './enemy';
import Graphics  from './graphics';
import Info      from './info';
import Messenger from './messenger';
import Player    from './player';

function decorateMessage(message) {
  const player = this.fetchPlayer(message.user);
  let label;

  if (this.cb.settings.graphics === 'sprite') {
    label = player.emoji ? this.graphics.render(player.emoji) : `:: Lv. ${player.level}`;
  } else if (this.cb.settings.graphics === 'icon') {
    label = player.emoji ? `:: Lv. ${player.level} ${this.graphics.render(player.emoji)}` : `:: Lv. ${player.level}`;
  } else {
    label = `:: Lv. ${player.level} ${startCase(player.characterClass) || 'Adventurer'}`;
  }

  message.m = `${label} :: ${message.m}`;

  return message;
}

/**
 * Execute an attack against an enemy by a player
 * @private
 * @param   {Object} player - Attacking player
 * @param   {number} amount - Damage to apply
 * @returns {Object} Attack result
 */
function attack(player, damage) {
  const previousLevel = player.level;
  const result = player.attack(this.enemy, damage);
  const levelDifference = player.level - previousLevel;
  const criticalHits = damage >= 15 ? result.rolls.filter(r => r >= 18) : 0;

  // Track cummulative damage
  this.totalDamage += damage;

  // Announce attack
  if (typeof player.damageText === 'function') {
    this.messenger.say(player.damageText(this.enemy, damage));
  } else {
    this.messenger.say(`${player.name} attacks the ${this.enemy.name}!`);
  }

  // Announce and log critical hit prizes
  if (criticalHits.length && player.characterClass) {
    const label = player.ability.type === 'preset'
      ? player.ability.effect
      : sample([
        this.prizes.bardicSong,
        this.prizes.command,
        this.prizes.calledShot
      ]);

    this.logReward(player.name, `${label} (x${criticalHits.length})`);

    this.messenger.announce(`
      ${player.critText()}
      ${this.graphics.render('crit')} ${this.graphics.render('crit')} ( ͡° ͜ʖ ͡°) Prize Awarded: ${label} (x${criticalHits.length}) ( ͡° ͜ʖ ͡°) ${this.graphics.render('crit')} ${this.graphics.render('crit')}
    `);
  }

  // Player levels up
  if (levelDifference) {
    const loot = Array.from(new Array(levelDifference), () => sample(this.prizes.loot));

    // Award the player loot
    player.addItems(loot);

    // Make announcements
    this.messenger.announce(`
      ${this.graphics.render('level-up')} ${player.name} has reached Level ${player.level}! ${this.graphics.render('level-up')}
      ${this.graphics.render('treasure')} Items acquired by ${player.name}: ${Player.printItems(loot)} ${this.graphics.render('treasure')}
    `);

    this.messenger.alert(`
      -----------------------------------------------------------------------------------------------------
      The following items have been added to your inventory:
      ${Player.printItems(loot)}.
      (Type /use item_name to use an item)
      -----------------------------------------------------------------------------------------------------
    `, player.name);
  }

  return result;
}

/**
 * Handle users entering
 * @param   {string} entrance - Chat entrance info
 * @returns {undefined}
 */
function handleEnter(entrance) {
  let player;

  if (!this.players.has(entrance.user)) {
    player = this.addPlayer(entrance.user);
  } else {
    player = this.players.get(entrance.user);
  }

  this.info.showWelcome(player);
}

/**
 * Handle chat messages
 * @private
 * @param   {Object} message - Chat message
 * @returns {Object} Chat message
 */
function handleMessage(message) {
  const cmdExecution = this.command.evaluate(message);

  if (cmdExecution !== null) {
    return Object.assign(message, { 'X-Spam': true });
  }

  return decorateMessage.call(this, message);
}

/**
 * Handle chat tips
 * @this     Game
 * @param   {Object} tip - Hashmap of tip properties
 * @returns {undefined}
 */
function handleTip(tip) {
  const user   = tip.from_user;
  const player = this.fetchPlayer(user);
  const previousHP = this.enemy.hp;

  // Help out players that haven't picked a class yet
  if (!player.characterClass) {
    this.info.showClasses(player);
  }

  // Execute the attack
  attack.call(this, player, tip.amount);

  // Player drops enemy
  if (this.enemy.hp === 0) {
    // Determine number of kills (goals reached) and any leftover damage to apply
    const kills = 1 + Math.floor((tip.amount - previousHP) / this.enemy.maxHP);
    const leftoverDamage = (tip.amount - previousHP) % this.enemy.maxHP;

    // Announce the kill
    this.messenger.announce(`${player.name} has slain the ${this.enemy.name}!`);

    this.totalKills += kills;
    this.spawnEnemy();
    this.enemy.damage(leftoverDamage);
  }

  this.cb.drawPanel();
}

/**
 * @class Game
 */
class Game {
  /**
   * @constructs Game
   * @param {Object} cb - Chaturbate interface
   */
  constructor(cb) {
    /** @type {Object} */
    this.cb = cb;

    /** @type {Enemy|null} */
    this.enemy = null;

    /** @type {Map} */
    this.players = new Map();

    /** @type {Object} */
    this.prizes = {};

    /** @type {Array.<Object>} */
    this.winners = [];

    /** @type {Command} */
    this.command = new Command();

    /** @type {Graphics} */
    this.graphics = new Graphics(cb);

    /** @type {Messenger} */
    this.messenger = new Messenger(cb);

    /** @type {Info} */
    this.info = new Info(this.messenger, this, this.graphics);

    /** @type {number} */
    this.totalDamage = 0;

    /** @type {number} */
    this.totalKills = 0;

    /** @type {Array} */
    this.rewardLog = [];
  }

  showInfo() {
    this.messenger.say(`Running Battlebate v${VERSION}`);
    return this;
  }

  registerCommands() {
    this.command.register('/class', [{ name: 'heroClass', type: 'directive', values: ['bard', 'fighter', 'ranger', 'warlock'] }], (message, command, args) => {
      const heroClass = args.heroClass;
      const player = this.fetchPlayer(message.user);

      if (heroClass) {
        player.changeClass(heroClass);
        this.messenger.whisper(`You have become a ${heroClass} - type /help for the full guide.`, message.user);
      } else {
        this.messenger.whisper(`Unknown hero class selected for /class command. Valid options are: fighter, ranger, warlock, bard.`, message.user);
      }
    });

    this.command.register('/use', [{ name: 'item', type: 'any', values: [] }], (message, command, args) => {
      const player = this.fetchPlayer(message.user);
      const item = args.item;

      if (item && player.inventory.some(i => i.toLowerCase() === item.toLowerCase())) {
        player.useItem(item);
        this.logReward(player.name, item);

        this.messenger.announce(`
          ${this.graphics.render('item')} ${message.user} uses an item: "${item}"! ${this.graphics.render('item')}
        `);
      } else {
        this.messenger.whisper(`
          Looks like you don't have that item in your inventory...
          Current inventory: ${Player.printItems(player.inventory)}
        `, message.user);
      }
    });

    this.command.register('/info', null, (message) => {
      const player = this.fetchPlayer(message.user);
      this.info.showPlayerInfo(player);
    });

    this.command.register('/help', [{ name: 'audience', type: 'directive', values: ['all'] }], (message, command, args) => {
      const audience = args.audience;
      const player = this.fetchPlayer(message.user);

      if (audience !== 'all') {
        this.info.showHelp(player);
        return;
      }

      if (message.is_mod || message.user === this.cb.room_slug) {
        this.info.showWelcome();
      } else {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
      }
    });

    this.command.register('/classes', [{ name: 'audience', type: 'directive', values: ['all'] }], (message, command, args) => {
      const audience = args.audience;
      const player = this.fetchPlayer(message.user);

      if (audience !== 'all') {
        this.info.showClasses(player);
        return;
      }

      if (message.is_mod || message.user === this.cb.room_slug) {
        this.info.showClasses();
      } else {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
      }
    });

    this.command.register('/graphics', [{ name: 'level', type: 'directive', values: ['sprite', 'icon', 'text'] }], (message, command, args) => {
      if (!message.is_mod && message.user !== this.cb.room_slug) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
        return;
      }

      if (args.level === null) {
        this.messenger.whisper('You dun goofed. Try /graphics sprite|icon|text', message.user);
        return;
      }

      this.cb.settings.graphics = args.level;
      this.messenger.whisper(`Graphics level has been to changed to "${args.level}".`, message.user);
    });

    this.command.register('/goal', [{ name: 'amount', type: 'number', values: [] }], (message, command, args) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
        return;
      }

      const amount = Number(args.amount);

      if (amount && amount > 0) {
        this.totalKills = 0;
        this.totalDamage = 0;
        this.cb.settings.goal = amount;

        this.spawnEnemy();
        this.cb.drawPanel();
      } else {
        this.messenger.whisper('Choose a token amount greater than 0 (e.g. /goal 1000)', message.user);
      }
    });

    this.command.register('/rewards', null, (message, command) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
        return;
      }

      this.info.showRewards();
    });

    this.command.register('/save', null, (message, command) => {
      if (!(message.is_mod || message.user === this.cb.room_slug)) {
        this.messenger.whisper('Only the broadcaster or mods can do that', message.user);
        return;
      }

      const players = new Map([...this.players].filter(([key, value]) => value.xp > 0));
      this.messenger.say(new Buffer(JSON.stringify(players)).toString('base64'), message.user);
    });
  }

  setReminder(reminder = null) {
    // Every 25 minutes, remind players with unused items about them
    if (reminder === 'unused-items') {
      setTimeout(() => {
        this.players.forEach((player, name) => {
          if (player.inventory.length) {
            this.messenger.announce(`
              Your inventory: ${Player.printItems(player.inventory)}
              ::: Type /use item_name to expend an item (e.g. /use ${player.inventory[0]}) :::
            `, player.name);
          }
        });

        this.setReminder('unused-items');
      }, 1800000);
    }

    // Every 15 minutes, remind players that the game is running and they can participate
    if (reminder === 'game-running') {
      setTimeout(() => {
        this.messenger.announce(`
          This room is running Battlebate, the Chaturbate fantasy role-playing game.
          Type /help to learn more about it.
        `);

        this.setReminder('game-running');
      }, 900000);
    }

    return this;
  }

  /**
   * Replaces current enemy with a newly generated one
   * @returns {Game} (self)
   */
  spawnEnemy() {
    this.enemy = new Enemy({ hp: this.cb.settings.goal });
    const graphic = this.graphics.render(this.enemy.graphic);

    this.messenger.taunt(`${graphic}...suddenly, a vile ${this.enemy.name} appears! Dare you challenge its might?`);

    return this;
  }

  /**
   * Setup game prizes
   * @returns {Game} `Game` instance
   */
  setupPrizes() {
    const settings = this.cb.settings;
    const settingsProperties = Object.keys(settings);

    this.prizes.loot = settingsProperties.filter(s => /loot/.test(s) && settings[s].trim().length).map(s => this.cb.settings[s]);

    this.prizes.bardicSong = settings.bardic_song_effect;
    this.prizes.command    = settings.command_effect;
    this.prizes.calledShot = settings.called_shot_target;

    Player.setupAbility('bardic-song', this.prizes.bardicSong);
    Player.setupAbility('command', this.prizes.command);
    Player.setupAbility('called-shot', this.prizes.calledShot);

    return this;
  }

  /**
   * Set the required amount of experience points players need to level up
   * @returns {Game} `Game` instance
   */
  setupLevelUpRequirement() {
    const xp = this.cb.settings.level_up_requirement;

    Player.setLevelUpRequirement(xp);
    return this;
  }

  /**
   * Set the required amount of experience points players need to pass ranks
   * @returns {Game} `Game` instance
   */
  setupRanks() {
    const legendaryRankXp = this.cb.settings.legendary_rank_xp;
    const eliteRankXp = this.cb.settings.elite_rank_xp;

    Player.setRanks(legendaryRankXp, eliteRankXp);
    return this;
  }

  /**
   * Add a given player to the game, by name
   * @param   {string} user           - The username of the player to add
   * @param   {string} characterClass - Name of the player's selected character class
   * @returns {Game} `Game` instance
   */
  addPlayer(user, characterClass) {
    const player = new Player(user);

    this.players.set(user, player);
    return player;
  }

  /**
   * Keeps track of critical hits and item usage
   * @param {string} user - Username of the rewarded player
   * @param {string} label - Description/name of the reward
   * @returns {Game} (self)
   */
  logReward(user, label) {
    this.rewardLog.push({
      user,
      label,
      time: new Date()
    });

    return this;
  }

  /**
   * Import the save from save input
   * @returns {Game} `Game` instance
   */
  importSave() {
    const save = this.cb.settings.save;
    if (save) {
      const schema = Joi.array().items(
        Joi.array().items(
          Joi.string(),
            Joi.object().keys({
              characterClass: Joi.string().valid('bard', 'fighter', 'ranger', 'warlock'),
              inventory: Joi.array().items(Joi.string()).required,
              xp: Joi.number().required()
            })
        )
      );
      try {
        const jsonSave = JSON.parse(new Buffer(save, 'base64').toString('utf8'));
        Joi.validate(jsonSave, schema, (err, value) => {
          if (err) {
            this.messenger.whisper('The imported save is not a valid save', this.cb.room_slug);
          } else {
            jsonSave.forEach(([name, attrs]) => {
              const player = new Player(name);
              player.xp = attrs.xp;
              player.inventory = attrs.inventory;

              if (attrs.characterClass) {
                player.changeClass(attrs.characterClass);
              }

              this.players.set(name, player);
            });
          }
        });
      } catch (err) {
        this.messenger.whisper('An error occurred when importing your saved game code. Make sure it\'s valid JSON.', this.cb.room_slug);
      }
    }
    return this;
  }

  /**
   * Gets a player by username, adding to the game if they're not already playing
   * @param {string} user - Username of the given user
   * @returns {Player} The given player
   */
  fetchPlayer(user) {
    return this.players.has(user) ? this.players.get(user) : this.addPlayer(user);
  }

  taunt() {
    const graphic = this.graphics.render(this.enemy.graphic);
    const message = this.enemy.taunt();
    const action  = sample([
      'gazes at the party of adventurers and utters',
      'regards the heroes with a hateful gaze and bellows',
      'glares at the bold adventurers and calls out',
      'flares up with anger and shouts',
      'studies the humans standing before it, then exclaims'
    ]);

    // Taunt once every 4 minutes
    setTimeout(() => {
      this.taunt();
    }, 240000);

    this.messenger.taunt(`${graphic} :: The ${this.enemy.name} ${action}, "${message}"`);

    return this;
  }

  /**
   * Start the game, binding event handlers
   * @returns {Game} `Game` instance
   */
  start() {
    // Setup options dropdowns
    this.cb.settings_choices = [
      { name: 'goal', label: 'Token Goal (auto-repeats - update via /goal)', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 500 },
      { name: 'level_up_requirement', label: 'Tokens needed to level up (random prize item)', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 150 },
      { name: 'legendary_rank_xp', label: 'Tokens needed to level pass legendary rank', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 1000 },
      { name: 'elite_rank_xp', label: 'Tokens needed to level pass elite rank', type: 'int', minValue: 1, maxValue: 100000, defaultValue: 500 },
      { name: 'loot_1', label: 'Level-up Item #1 (required)', type: 'str', defaultValue: 'Magic Ball Gag' },
      { name: 'loot_2', label: 'Level-up Item #2', type: 'str', defaultValue: 'Scroll of Conjure Butt Plug', required: false },
      { name: 'loot_3', label: 'Level-up Item #3', type: 'str', defaultValue: 'Flask of Body Oil', required: false },
      { name: 'bardic_song_effect', label: 'Bard Critical Hit Prize (Bardic Song)', type: 'str', defaultValue: `sexy dancing` },
      { name: 'command_effect', label: 'Warlock Critical Hit Prize (Command)', type: 'str', defaultValue: '10 spanks or mute/gag' },
      { name: 'called_shot_target', label: 'Ranger Critical Hit Prize (Called Shot)', type: 'str', defaultValue: 'flash of player\'s choice' },
      { name: 'graphics', label: `Graphics Level`, type: 'choice', choice1: 'sprite', choice2: 'icon', choice3: 'text', defaultChoice: 'icon' },
      { name: 'show_total_tokens', label: 'Show Total Tokens', type: 'choice', choice1: 'yes', choice2: 'no', defaultChoice: 'yes' },
      { name: 'save', label: 'Saved Game Code', type: 'str', defaultValue: '', required: false  }
    ];

    this.importSave();

    // Setup prizes (loot, etc.)
    this.setupPrizes();

    // Setup requisite xp amount for level-up
    this.setupLevelUpRequirement();

      // Setup requisite xp amount for pass ranks
    this.setupRanks();

    // Display game info
    this.showInfo();

    // Setup enemy to fight
    this.spawnEnemy();

    // Kick off taunts
    this.taunt();

    // Show welcome message
    this.info.showWelcome();

    // Display enemy info
    this.cb.onDrawPanel(user => ({
      template: '3_rows_11_21_31',
      row1_value: `Lv. ${this.enemy.level} ${startCase(this.enemy.name)}`,
      row2_value: `Hitpoints \u2022 ${this.enemy.hp} tokens until defeat`,
      row3_value: this.cb.settings.show_total_tokens === 'yes'
        ? `Progress \u2022 ${this.totalKills} monsters slain (${this.totalDamage} tokens)`
        : `Progress \u2022 ${this.totalKills} monsters slain`
    }));

    // Setup chat commands
    this.registerCommands();

    // Setup reminders
    this.setReminder('unused-items');
    this.setReminder('game-running');

    // Attach event handlers
    this.cb.onTip(handleTip.bind(this));
    this.cb.onEnter(handleEnter.bind(this));
    this.cb.onMessage(handleMessage.bind(this));

    return this;
  }
}

export default Game;
