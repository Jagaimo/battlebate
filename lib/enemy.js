/**
 * @ts-check
 * @overview The enemy player characters face off against, i.e. the main goal
 * @module   enemy
 * @requires lodash.sample
 */

'use strict';

import sample from 'lodash.sample';

const TAUNTS = new Map()
  .set('fiery', [
    'I am destruction incarnate!',
    'I must...destroy!',
    'All shall burn!',
    'Your ashes will litter the battlefield!',
    'You will all be reduced to cinders!',
    'You burn well.'
  ])
  .set('slow', [
    'The ground...trembles...with every step...',
    'Must...obliterate!',
    'Back, creatures...!',
    'Inexorable...vengeance...',
    'I will leave devastation...in my wake...',
    'You must be culled...',
    'I will tear down...what you have built...',
    'I will not...be turned aside...!'
  ])
  .set('gruff', [
    'I break you!',
    'Gonna clobber da whole lot of ya!',
    'Time to bust some skulls!',
    'Lemme at \'em!',
    'Stand still so I can kill ya!',
    'Time for killin\'!'
  ])
  .set('ghastly', [
    'I walk again...',
    'Good...more walking sacrifices...',
    'All...shall...perish!',
    'More souls to feast upon!',
    'Yes...fight! It will make your souls all the sweeeter...',
    'Your cries for pity will not move me...',
    'Do not die too easily...I want you to suffer...',
    'Ah...a fresh crop of victims...',
    'You are wise to fear the dead...',
    'Let the living tremble before the dead...'
  ])
  .set('warrior', [
    'Pathetic fools!',
    'I will tear you limb from limb!',
    'Can you weaklings not fight any better?!',
    'You will be broken!',
    'I will have last blood!',
    'Who dies first?!'
  ]);


/**
 * D&D monster names, loosely organized by challenge rating
 * @see {@link https://media.wizards.com/2014/downloads/dnd/MM_MonstersCR.pdf}
 * @type {Map}
 */
const MONSTERS = new Map()
  .set('weak', [
    { name: 'ghost', graphic: 'ghost', speech: 'ghastly' },
    { name: 'lizardman', graphic: 'lizardman', speech: 'gruff' },
    { name: 'skeleton archer', graphic: 'skeleton-archer', speech: 'warrior' },
    { name: 'skeleton swordsman', graphic: 'skeleton-swordsman', speech: 'warrior' }
  ])
  .set('strong', [
    { name: 'fire elemental', graphic: 'fire-elemental', speech: 'fiery' },
    { name: 'half-dragon', graphic: 'half-dragon', speech: 'warrior' },
    { name: 'myrmidon', graphic: 'myrmidon', speech: 'warrior' },
    { name: 'ogre', graphic: 'ogre', speech: 'gruff' },
    { name: 'orc chieftain', graphic: 'orc-chieftain', speech: 'gruff' },
    { name: 'troll', graphic: 'troll', speech: 'gruff' },
    { name: 'wraith', graphic: 'wraith', speech: 'ghastly' }
  ])
  .set('epic', [
    { name: 'ancient dragon', graphic: 'ancient-dragon', speech: 'fiery' },
    { name: 'awakened tree', graphic: 'awakened-tree', speech: 'slow' },
    { name: 'draugr', graphic: 'draugr', speech: 'ghastly' },
    { name: 'lich', graphic: 'lich', speech: 'ghastly' },
    { name: 'undead dragon', graphic: 'undead-dragon', speech: 'ghastly' }
  ]);

function generatePersona(hp) {
  let personas;

  if (hp < 1000) {
    personas = MONSTERS.get('weak');
  } else if (hp >= 1000 && hp <= 2000) {
    personas = MONSTERS.get('strong');
  } else {
    personas = MONSTERS.get('epic');
  }

  return sample(personas);
}


/**
 * @class Enemy
 */
class Enemy {
  /**
   * @constructs Enemy
   * @param {Object}  attributes - Initial enemy properties
   * @param {string}  attributes.name - Name of the enemy
   * @param {string}  attributes.emoji - Icon/visual
   * @param {number}  attributes.hp - Total hitpoints
   */
  constructor(attributes) {
    const persona = generatePersona(attributes.hp);

    /** @type {string} */
    this.name = persona.name;

    /** @type {string} */
    this.graphic = persona.graphic;

    /** @type {number} */
    this.hp = attributes.hp;

    /** @type {number} */
    this.maxHP = attributes.hp;

    /** @type {Array} */
    this.taunts = TAUNTS.get(persona.speech);
  }

  get level() {
    const level = Math.floor(this.maxHP / 100 / 1.5) || 1;

    return level > 20 ? 20 : level;
  }

  /**
   * Damage the enemy
   * @param   {number} amount - Amount of damage to take
   * @returns {Enemy} `Enemy` instance
   */
  damage(amount) {
    this.hp -= amount;

    if (this.hp < 0) {
      this.hp = 0;
    }

    return this;
  }

  taunt() {
    return sample(this.taunts);
  }
}

export default Enemy;
