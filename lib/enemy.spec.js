/* eslint-env mocha */
/* @ts-check */

'use strict';

import Enemy from './enemy';

describe('Enemy', () => {
  describe('initialize', () => {
    const enemy = new Enemy({ hp: 1000 });

    it('should return an instance of `Player`', () => {
      enemy.should.be.instanceOf(Enemy);
    });

    it('should assign enemy properties', () => {
      enemy.hp.should.equal(1000);
    });
  });

  describe('.name|.graphic|.level', () => {
    let enemy;

    context('when hp is < 100', () => {
      beforeEach(() => enemy = new Enemy({ hp: 50 }));

      it('should be a weak enemy', () => {
        enemy.name.should.be.oneOf(['ghost', 'lizardman', 'skeleton archer', 'skeleton swordsman']);
        enemy.graphic.should.be.oneOf(['ghost', 'lizardman', 'skeleton-archer', 'skeleton-swordsman']);
        enemy.level.should.equal(1);
      });
    });

    context('when hp is < 1000', () => {
      beforeEach(() => enemy = new Enemy({ hp: 500 }));

      it('should be a weak enemy', () => {
        enemy.name.should.be.oneOf(['ghost', 'lizardman', 'skeleton archer', 'skeleton swordsman']);
        enemy.graphic.should.be.oneOf(['ghost', 'lizardman', 'skeleton-archer', 'skeleton-swordsman']);
        enemy.level.should.equal(3);
      });
    });

    context('when hp is >= 1000 and <= 2000', () => {
      beforeEach(() => enemy = new Enemy({ hp: 1000 }));

      it('should be a strong enemy', () => {
        enemy.name.should.be.oneOf(['fire elemental', 'half-dragon', 'myrmidon', 'ogre', 'orc chieftain', 'troll', 'wraith']);
        enemy.graphic.should.be.oneOf(['fire-elemental', 'half-dragon', 'myrmidon', 'ogre', 'orc-chieftain', 'troll', 'wraith']);
        enemy.level.should.equal(6);
      });
    });

    context('when hp is > 2000', () => {
      beforeEach(() => enemy = new Enemy({ hp: 3500 }));

      it('should be an epic enemy', () => {
        enemy.name.should.be.oneOf(['ancient dragon', 'awakened tree', 'draugr', 'lich', 'undead dragon']);
        enemy.graphic.should.be.oneOf(['ancient-dragon', 'awakened-tree', 'draugr', 'lich', 'undead-dragon']);
        enemy.level.should.equal(20);
      });
    });
  });

  describe('#damage', () => {
    context('when the damage does not exceed current hp', () => {
      const enemy = new Enemy({ hp: 1000 });
      enemy.damage(10);

      it('should reduce the total HP', () => {
        enemy.hp.should.equal(990);
      });
    });

    context('when the damage exceeds current hp', () => {
      const enemy = new Enemy({ hp: 1000 });
      enemy.damage(1000);

      it('should reset damage to zero', () => {
        enemy.hp.should.equal(0);
      });
    });
  });
});
