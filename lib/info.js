/**
 * @ts=check
 * @overview Provides game state information and user help
 * @module   info
 * @requires Player
 * @requires moment/min/moment.min
 */

'use strict';

import moment from 'moment/min/moment.min';

import Player from './player';

const WELCOME = (game, graphics) => (
  `
    -----------------------------------------------------------------------------------------------------
    ${graphics.render('logo')} Battlebate - The Chaturbate Role-Playing Game ${graphics.render('logo')}
    -----------------------------------------------------------------------------------------------------
    ${graphics.render('bullet')} Tip to attack the monster and help save the model.
    ${graphics.render('bullet')} Whenever you tip 25+, you may land a Critical Hit and win a free prize. More tokens = better odds.
    ${graphics.render('bullet')} As you tip (attack), you gain experience points (XP) and will level up every ${game.cb.settings.level_up_requirement} tokens.
    ${graphics.render('bullet')} Leveling up gains you free prize items to use and a higher Critical Hit chance.
    -----------------------------------------------------------------------------------------------------
    ::: Use the /class command to pick a hero class and win special prizes!
    -----------------------------------------------------------------------------------------------------
    ${graphics.render('ranger-novice')} /class ranger - Become a precision hunter.................................Chance to win: ${game.prizes.calledShot}
    ${graphics.render('bard-novice')} /class bard - Wield music as your magic.................................Chance to win: ${game.prizes.bardicSong}
    ${graphics.render('warlock-novice')} /class warlock - Dominate minds and souls...............................Chance to win: ${game.prizes.command}
    ${graphics.render('fighter-novice')} /class fighter - Master all forms of combat..................................Chance to win: [any of the above]
  `.trim()
);

const PLAYER_INFO = (player) => (
  `
    Class: ${player.characterClass || 'None'}
    Level: ${player.level}
    XP: ${player.xp}
    Inventory: ${Player.printItems(player.inventory)}
  `.trim()
);

const REWARDS = (game) => {
  const now = moment();

  const rewards = game.rewardLog.filter((reward) => (
    now.diff(reward.time, 'hours') <= 1
  )).slice(-15).reverse();

  const prizeList = rewards.map((reward) => (
    `* ${reward.label} - ${reward.user} (${moment(reward.time).fromNow()})`
  )).join('\n');

  return `
    ::: Recently won prizes and expended items :::
    ${prizeList.length ? prizeList : '(not a damn thing)'}
  `.trim();
};

const CLASS_INFO = (game, graphics) => (
  `
    -----------------------------------------------------------------------------------------------------
    ::: Use the /class command to pick a hero class and win special prizes!
    -----------------------------------------------------------------------------------------------------
    ${graphics.render('ranger-novice')} /class ranger - Become a precision hunter.................................Chance to win: ${game.prizes.calledShot}
    ${graphics.render('bard-novice')} /class bard - Wield music as your magic.................................Chance to win: ${game.prizes.bardicSong}
    ${graphics.render('warlock-novice')} /class warlock - Dominate minds and souls...............................Chance to win: ${game.prizes.command}
    ${graphics.render('fighter-novice')} /class fighter - Master all forms of combat..................................Chance to win: [any of the above]
  `.trim()
);

const HELP = (game) => (
  `
    -----------------------------------------------------------------------------------------------------
    ::: How to Play Battlebate :::
    -----------------------------------------------------------------------------------------------------
    1. Pick a hero class/role: Type /class bard|fighter|ranger|warlock.
    2. Tip as usual to attack the monster and deal damage.
    3. The more you tip, the better your chances of winning a special prize (aka Critical Hit).
    4. Level up for every ${game.cb.settings.level_up_requirement} tokens you spend, gaining a special prize item you can use whenever.
    -----------------------------------------------------------------------------------------------------
    ::: Chat Commands :::
    -----------------------------------------------------------------------------------------------------
    * /use [item]...................Use an item in your inventory, e.g. /use flask of body oil
    * /class [name]..............Change to hero class with the given name, e.g. /class ranger
    * /info.............................Show player character info
    * /help............................Show this help menu
    * /help all........................Show everyone help menu (mods and model only)
    * /classes.........................Show info about the different hero classes
    * /classes all.....................Show everyone info about the different hero classes (mods and model only)
    -----------------------------------------------------------------------------------------------------
    ::: Broadcaster + Mod Commands :::
    * /goal [amount]...............Start a goal of the specified amount of tokens, e.g. /goal 1000
    * /rewards.....................Show recently won prizes and used items
    * /graphics [level]............Change the graphics level (sprite, icon, or text)
    -----------------------------------------------------------------------------------------------------
    ::: Hero Classes :::
    -----------------------------------------------------------------------------------------------------
    /class ranger - Become a precision hunter..............................Chance to win: ${game.prizes.calledShot}
    /class bard - Wield music as your magic.................................Chance to win: ${game.prizes.bardicSong}
    /class warlock - Dominate the minds and souls of others.....Chance to win: ${game.prizes.command}
    /class fighter - Master all forms of combat..............................Chance to win: [any of the above]
  `.trim()
);

/**
 * @class Info
 */
class Info {
  /**
   * @constructs Info
   * @param {Messenger} messenger - Injected `Messenger` dependency
   * @param {Game} game - Injected `Game` dependency
   * @param {Graphics} graphics - Injected `Graphics` dependency
   */
  constructor(messenger, game, graphics) {
    /** @type {Messenger} */
    this.messenger = messenger;

    /** @type {Game} */
    this.game = game;

    /** @type {Graphics} */
    this.graphics = graphics;
  }

  /**
   * Send a welcome message to inform users of the game
   * @param   {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showWelcome(player = {}) {
    this.messenger.announce(WELCOME.call(null, this.game, this.graphics), player.name);

    return this;
  }

  /**
   * Show current player state
   * @param   {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showPlayerInfo(player) {
    const message = PLAYER_INFO.call(null, player);

    this.messenger.whisper(message, player.name);

    return this;
  }

  /**
   * Show prize winner and time information
   * @returns {Info} `Info` instance
   */
  showRewards() {
    this.messenger.announce(REWARDS.call(null, this.game));

    return this;
  }

  /**
   * Show information about hero classes
   * @param   {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showClasses(player = {}) {
    this.messenger.announce(CLASS_INFO.call(null, this.game, this.graphics), player.name);

    return this;
  }

  /**
   * Show the help menu to a given user
   * @param   {Player} [player] - Game participant to message
   * @returns {Info} `Info` instance
   */
  showHelp(player = {}) {
    this.messenger.say(HELP.call(null, this.game), player.name);

    return this;
  }
}

export {
  WELCOME,
  CLASS_INFO,
  PLAYER_INFO,
  REWARDS,
  HELP
};

export default Info;
