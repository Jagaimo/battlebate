/* eslint-env mocha */

'use strict';

import { default as Messenger, colors } from './messenger';

describe('Messenger', () => {
  const cb = {
    sendNotice: global.sandbox.stub()
  };

  let messenger;

  beforeEach(() => {
    messenger = new Messenger(cb);
  });

  describe('initialize', () => {
    it('should inejct the `cb` dependency', () => {
      messenger.cb.should.equal(cb);
    });
  });

  describe('#whisper', () => {
    context('when no user is specified', () => {
      beforeEach(() => {
        messenger.whisper('<message>');
      });

      it('should send a message to the general chat', () => {
        cb.sendNotice.should.have.been.calledWith('<message>', '', null, colors.get('whisper-foreground'), 'bold');
      });
    });

    context('when a user is specified', () => {
      beforeEach(() => {
        messenger.whisper('<message>', '<user>');
      });

      it('should send a message to the user', () => {
        cb.sendNotice.should.have.been.calledWith('<message>', '<user>', null, colors.get('whisper-foreground'), 'bold');
      });
    });
  });

  describe('#say', () => {
    beforeEach(() => {
      messenger.say('<message>');
    });

    it('should make an announcement to the general chat', () => {
      cb.sendNotice.should.have.been.calledWith('<message>', '');
    });
  });

  describe('#alert', () => {
    beforeEach(() => {
      messenger.alert('<message>', '<user>');
    });

    it('should send an alert to the user', () => {
      cb.sendNotice.should.have.been.calledWith('<message>', '<user>', null, colors.get('alert-foreground'), 'bold');
    });
  });

  describe('#announce', () => {
    beforeEach(() => {
      messenger.announce('<message>');
    });

    it('should broadcast a taunt to the general chat', () => {
      cb.sendNotice.should.have.been.calledWith('<message>', '', null, colors.get('announce-foreground'), 'bold');
    });
  });
});
