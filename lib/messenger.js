/**
 * @overview Sends chat messages and announcements
 * @module   messenger
 */

'use strict';

/**
 * Chat message colors
 */
const COLORS = new Map()
  .set('announce-foreground', '#34415e')
  .set('say-foreground', '#007FAA')
  .set('alert-foreground', '#FF6347')
  .set('taunt-foreground', '#870C25')
  .set('whisper-foreground', '#DB0A5B')
  .set('body', '#1C2833');

/**
 * @class Messenger
 */
class Messenger {
  /**
   * @constructs Messenger
   * @param {Object} cb - Chaturbate interface
   */
  constructor(cb) {
    /** @type {Object} */
    this.cb = cb;
  }

  /**
   * Output basic chat message
   * @param   {string} message - Body text
   * @param   {string} [username] - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  whisper(message, username = '') {
    const color = COLORS.get('whisper-foreground');
    this.cb.sendNotice(message.trim(), username, null, color, 'bold');

    return this;
  }

  /**
   * Output a more noticeable message
   * @param   {string} message - Header text
   * @param   {string} [username] - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  say(message, username = '') {
    this.cb.sendNotice(message.trim(), username, null, COLORS.get('say-foreground'), 'bold');
    return this;
  }

  /**
   * Output a heavily-emphasized message to a specific
   * @param   {string} message - Header text
   * @param   {string} [username] - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  alert(message, username = '') {
    this.cb.sendNotice(message.trim(), username, null, COLORS.get('alert-foreground'), 'bold');
    return this;
  }

  /**
   * Output a heavily-emphasized message to all
   * @param   {string} message - Header text
   * @param   {string} [username] - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  announce(message, username = '') {
    this.cb.sendNotice(message.trim(), username, null, COLORS.get('announce-foreground'), 'bold');
    return this;
  }

  /**
   * ...
   * @param   {string} message - Header text
   * @param   {string} [username] - Username of the message recipient
   * @returns {Messenger} `Messenger` instance
   */
  taunt(message, username = '') {
    this.cb.sendNotice(message.trim(), username, null, COLORS.get('taunt-foreground'), 'bold');
    return this;
  }
}

export default Messenger;
export { COLORS as colors };
