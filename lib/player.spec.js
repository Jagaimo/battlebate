/* eslint-env mocha */

'use strict';

import Enemy  from './enemy';
import Player from './player';

describe('Player', () => {
  describe('::setupAbility', () => {
    context(`when 'Bardic Song' is specified`, () => {
      it('should assign special effects', () => {
        Player.setupAbility('bardic-song', '<effect>');

        const player = new Player();

        player.changeClass('bard');
        player.ability.effect.should.eql('<effect>');
      });
    });

    context(`when 'Command' is specified`, () => {
      it('should assign special effects', () => {
        Player.setupAbility('command', '<effect>');

        const player = new Player();

        player.changeClass('warlock');
        player.ability.effect.should.eql('<effect>');
      });
    });

    context(`when 'Called Shot' is specified`, () => {
      it('should assign special effects', () => {
        Player.setupAbility('called-shot', '<effect>');

        const player = new Player();

        player.changeClass('ranger');
        player.ability.effect.should.eql('<effect>');
      });
    });
  });

  describe('::printItems', () => {
    let player;

    context('when the player has no items', () => {
      beforeEach(() => {
        player = new Player('<name>');
      });

      it('should show placeholder text', () => (
        Player.printItems(player.inventory).should.equal('(nothing)')
      ));
    });

    context('when the player has a single item', () => {
      beforeEach(() => {
        player = new Player('<name>');
        player.addItems(['<item.1>']);
      });

      it('should show items', () => (
        Player.printItems(player.inventory).should.equal('<item.1> (x1)')
      ));
    });

    context('when the player has multiple items', () => {
      beforeEach(() => {
        player = new Player('<name>');
        player.addItems(['<item.1>', '<item.2>', '<item.3>']);
      });

      it('should show a list of items', () => (
        Player.printItems(player.inventory).should.equal('<item.1> (x1), <item.2> (x1), <item.3> (x1)')
      ));
    });

    context('when the player has duplicate items', () => {
      beforeEach(() => {
        player = new Player('<name>');
        player.addItems(['<item.1>', '<item.1>']);
      });

      it('should show a list of items', () => (
        Player.printItems(player.inventory).should.equal('<item.1> (x2)')
      ));
    });
  });

  describe('initialize', () => {
    const player = new Player('<name>');

    it('should return an instance of `Player`', () => {
      player.should.be.instanceOf(Player);
    });

    it('should assign initial property values', () => {
      player.name.should.equal('<name>');
      player.level.should.equal(1);
      player.xp.should.equal(0);
      player.inventory.should.eql([]);
    });
  });

  describe('.level', () => {
    const player = new Player('<name>');

    it('should reflect current xp', () => {
      player.xp = 1000;
      player.level.should.equal(5);
    });
  });

  describe('.emoji', () => {
    it('should render a graphic to represent the player based on class and rank', () => {
      const classes = ['bard', 'ranger', 'fighter', 'warlock'];

      classes.forEach(heroClass => {
        const player = new Player('<name>');
        player.should.have.property('emoji', null);

        player.changeClass(heroClass);
        player.emoji.should.equal(`${heroClass}-novice`);

        player.xp += 500;
        player.emoji.should.equal(`${heroClass}-elite`);

        player.xp += 500;
        player.emoji.should.equal(`${heroClass}-legendary`);
      });
    });
  });

  describe('#changeClass', () => {
    const player = new Player('<name>');
    player.changeClass('bard');

    it('should assign the given character class', () => {
      player.characterClass.should.equal('bard');
    });

    it('should assign the corresponding special ability', () => {
      player.ability.name.should.equal('Bardic Song');
    });
  });

  describe('#addItems', () => {
    let player;

    beforeEach(() => {
      player = new Player('<name>');
    });

    it('should add a single item', () => {
      player.addItems(['<item>']);
      player.inventory.should.eql(['<item>']);
    });

    it('should add multiple items', () => {
      player.addItems(['<item.1>', '<item.2>', '<item.3>']);
      player.inventory.should.eql(['<item.1>', '<item.2>', '<item.3>']);
    });
  });

  describe('#useItem', () => {
    const player = new Player('<name>');
    player.inventory = ['<item>'];

    it('should expend the given item', () => {
      player.useItem('<item>');
      player.inventory.length.should.equal(0);
    });

    context('when the input is cased differently', () => {
      it('should expend the given item', () => {
        player.useItem('<ITEM>');
        player.inventory.length.should.equal(0);
      });
    });
  });

  describe('#attack', () => {
    const player = new Player('<name>');
    const enemy  = new Enemy({ hp: 1000 });

    it('should deal damage to the target', () => {
      const damage = global.sandbox.spy(enemy, 'damage');

      player.attack(enemy, 10);
      damage.should.have.been.calledWith(10);
    });

    it('should acrue experience points', () => {
      player.xp.should.equal(10);
    });

    it('should return a result object', () => {
      player.attack(enemy, 10).should.be.an('object');
    });

    it('should return the results no rolls', () => {
      const result = player.attack(enemy, 10);

      result.rolls.should.eql([]);
    });

    it('should return the result of a single roll', () => {
      const result = player.attack(enemy, 25);

      result.rolls.should.be.an('array');
      result.rolls.should.have.length(1);
    });

    it('should return the result of multiple rolls', () => {
      const result = player.attack(enemy, 50);

      result.rolls.should.be.an('array');
      result.rolls.should.have.length(2);
    });
  });
});
