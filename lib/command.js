/**
 * @ts-check
 * @overview Parses and fires callbacks in response to chat commands
 * @module   command
 * @requires assert
 */

'use strict';

import assert from 'assert';

const ERROR_INVALID_PATTERN = 'Invalid `pattern` argument for Command#register';
const ERROR_INVALID_PARAMS = 'Invalid `params` argument for Command#register';

const PATTERNS = new Map()
  .set('any', /.*/)
  .set('directive', /^\w+/)
  .set('string', /(?!^").+(?=")/)
  .set('assignment', /(?!^\w+\s*=\s*)\w+$/)
  .set('number', /^\d+/);

function getArguments(text, command) {
  const subtext = text.replace(RegExp(`^${command.pattern}\\s*`), '').trim();
  const params = command.params;
  const args = {};

  for (const param of params) {
    const typePattern = PATTERNS.get(param.type);

    if (typePattern.test(subtext)) {
      const value = subtext.match(typePattern)[0];
      args[param.name] = !param.values.length || ~param.values.indexOf(value) ? value : null;
    } else {
      args[param.name] = null;
    }
  }

  return args;
}

function hasCommand(text, command) {
  const pattern = `^${command.pattern}\\b\\s*`;
  return RegExp(pattern).test(text);
}

class Command {
  constructor() {
    this.registry = new Map();
  }

  evaluate(message) {
    let result = null;

    for (const command of this.registry.values()) {
      if (hasCommand(message.m, command)) {
        const args = command.params ? getArguments(message.m, command) : null;
        result = command.callback(message, command, args);

        break;
      }
    }

    return result;
  }

  register(pattern, params, callback) {
    assert(pattern.constructor === String, `${ERROR_INVALID_PATTERN}: ${JSON.stringify(pattern)}`);

    if (params) {
      assert(params.constructor === Array, `${ERROR_INVALID_PARAMS}: ${JSON.stringify(params)}`);
    }

    this.registry.set(pattern, {
      pattern,
      params,
      callback
    });

    return this;
  }
}

export {
  Command as default,
  ERROR_INVALID_PATTERN,
  ERROR_INVALID_PARAMS
};
