/* eslint-env mocha */

'use strict';

import moment from 'moment';
import Player from './player';

import {
  default as Info,
  CLASS_INFO,
  HELP
 } from './info';

function buildPlayer(options = {}) {
  return Object.assign({
    inventory: []
  }, options);
}

const messenger = {
  whisper: global.sandbox.stub(),
  announce: global.sandbox.stub(),
  say: global.sandbox.stub()
};

const game = {
  cb: {
    settings: {}
  },
  prizes: {
    calledShot: '<prize.1>',
    bardicSong: '<prize.2>',
    command: '<prize.3>'
  },
  rewardLog: []
};

const graphics = {
  render() {}
};

describe('Info', () => {
  let info;

  beforeEach(() => {
    info = new Info(messenger, game, graphics);
  });

  describe('initialize', () => {
    it('should inject the `messenger` dependency', () => (
      info.messenger.should.equal(messenger)
    ));

    it('should inject the `game` dependency', () => (
      info.game.should.equal(game)
    ));

    it('should inject the `graphics` dependency', () => (
      info.graphics.should.equal(graphics)
    ));
  });

  describe('#showPlayerInfo', () => {
    let player;

    beforeEach(() => {
      player = buildPlayer();

      global.sandbox.spy(Player, 'printItems');
      info.showPlayerInfo(player);
    });

    it('should call Player::printItems', () => {
      Player.printItems.should.have.been.calledWith(player.inventory);
    });
  });

  describe('#showRewards', () => {
    beforeEach(() => {
      const now = moment();

      const times = [
        now.clone().subtract(30, 'seconds'),
        now.clone().subtract(10, 'minutes'),
        now.clone().subtract(1, 'hours')
      ];

      info.game.rewardLog = [
        { user: '<player.3>', label: '<prize.3>', time: times[2] },
        { user: '<player.2>', label: '<prize.2>', time: times[1] },
        { user: '<player.1>', label: '<prize.1>', time: times[0] }
      ];

      info.showRewards();
    });

    it('should show a list of recently won prizes to general chat', () => {
      messenger.announce.should.have.been.calledWithExactly(`::: Recently won prizes and expended items :::
    * <prize.1> - <player.1> (a few seconds ago)
* <prize.2> - <player.2> (10 minutes ago)
* <prize.3> - <player.3> (an hour ago)`);
    });
  });

  describe('#showClasses', () => {
    context('when given a username', () => {
      beforeEach(() => {
        info.showClasses({ name: '<name>' });
      });

      it('should send the message to the specified user', () => (
        messenger.announce.should.have.been.calledWithExactly(CLASS_INFO.call(null, info.game, info.graphics), '<name>')
      ));
    });

    context('when not given a username', () => {
      beforeEach(() => {
        info.showClasses();
      });

      it('should send the message to general chat', () => (
        messenger.announce.should.have.been.calledWithExactly(CLASS_INFO.call(null, info.game, info.graphics), undefined)
      ));
    });
  });

  describe('#showHelp', () => {
    beforeEach(() => {
      info.showHelp({ name: '<name>' });
    });

    it('should show the given user the help menu', () => (
      messenger.say.should.have.been.calledWithExactly(HELP.call(null, info.game), '<name>')
    ));
  });
});
