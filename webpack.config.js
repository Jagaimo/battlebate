'use strict';

const IgnorePlugin          = require('webpack/lib/IgnorePlugin');
const OccurrenceOrderPlugin = require('webpack/lib/optimize/OccurrenceOrderPlugin');
const UglifyJsPlugin        = require('webpack/lib/optimize/UglifyJsPlugin');

module.exports = {
  entry: './lib/index.js',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      },
      {
        test: /\.json$/,
        loaders: ['json-loader']
      }
    ]
  },
  output: {
    filename: './dist/index.js'
  },
  plugins: [
    new IgnorePlugin(/\.\/locale$/),
    new OccurrenceOrderPlugin(),
    new UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false,
        ascii_only: true
      }
    })
  ]
};
