## Modules

<dl>
<dt><a href="#module_command">command</a></dt>
<dd><p>Parses and fires callbacks in response to chat commands</p>
</dd>
<dt><a href="#module_enemy">enemy</a></dt>
<dd><p>The enemy player characters face off against, i.e. the main goal</p>
</dd>
<dt><a href="#module_game">game</a></dt>
<dd><p>Main game module</p>
</dd>
<dt><a href="#module_graphics">graphics</a></dt>
<dd><p>Library of game graphics references (simple emoji codes) and text fallbacks</p>
</dd>
<dt><a href="#module_info">info</a></dt>
<dd><p>Provides game state information and user help</p>
</dd>
<dt><a href="#module_messenger">messenger</a></dt>
<dd><p>Sends chat messages and announcements</p>
</dd>
<dt><a href="#module_player">player</a></dt>
<dd><p>Game party member</p>
</dd>
</dl>

## Classes

<dl>
<dt><a href="#Enemy">Enemy</a></dt>
<dd></dd>
<dt><a href="#Game">Game</a></dt>
<dd></dd>
<dt><a href="#Info">Info</a></dt>
<dd></dd>
<dt><a href="#Messenger">Messenger</a></dt>
<dd></dd>
<dt><a href="#Player">Player</a></dt>
<dd></dd>
</dl>

<a name="module_command"></a>

## command
Parses and fires callbacks in response to chat commands

**Requires**: <code>module:assert</code>  
**Ts-check**:   
<a name="module_enemy"></a>

## enemy
The enemy player characters face off against, i.e. the main goal

**Requires**: <code>module:lodash.sample</code>  
**Ts-check**:   

* [enemy](#module_enemy)
    * [~Enemy](#module_enemy..Enemy)
        * [.damage(amount)](#module_enemy..Enemy+damage) ⇒ <code>[Enemy](#Enemy)</code>
    * [~MONSTERS](#module_enemy..MONSTERS) : <code>Map</code>

<a name="module_enemy..Enemy"></a>

### enemy~Enemy
**Kind**: inner class of <code>[enemy](#module_enemy)</code>  
<a name="module_enemy..Enemy+damage"></a>

#### enemy.damage(amount) ⇒ <code>[Enemy](#Enemy)</code>
Damage the enemy

**Kind**: instance method of <code>[Enemy](#module_enemy..Enemy)</code>  
**Returns**: <code>[Enemy](#Enemy)</code> - `Enemy` instance  

| Param | Type | Description |
| --- | --- | --- |
| amount | <code>number</code> | Amount of damage to take |

<a name="module_enemy..MONSTERS"></a>

### enemy~MONSTERS : <code>Map</code>
D&D monster names, loosely organized by challenge rating

**Kind**: inner constant of <code>[enemy](#module_enemy)</code>  
**See**: [https://media.wizards.com/2014/downloads/dnd/MM_MonstersCR.pdf](https://media.wizards.com/2014/downloads/dnd/MM_MonstersCR.pdf)  
<a name="module_game"></a>

## game
Main game module

**Requires**: <code>[command](#module_command)</code>, <code>[enemy](#module_enemy)</code>, <code>[graphics](#module_graphics)</code>, <code>[info](#module_info)</code>, <code>module:lodash.sample</code>, <code>module:lodash.startcase</code>, <code>[messenger](#module_messenger)</code>, <code>[player](#module_player)</code>  
**Ts-check**:   

* [game](#module_game)
    * [~Game](#module_game..Game)
        * [.spawnEnemy()](#module_game..Game+spawnEnemy) ⇒ <code>[Game](#Game)</code>
        * [.setupPrizes()](#module_game..Game+setupPrizes) ⇒ <code>[Game](#Game)</code>
        * [.setupLevelUpRequirement()](#module_game..Game+setupLevelUpRequirement) ⇒ <code>[Game](#Game)</code>
        * [.setupRanks()](#module_game..Game+setupRanks) ⇒ <code>[Game](#Game)</code>
        * [.addPlayer(user, characterClass)](#module_game..Game+addPlayer) ⇒ <code>[Game](#Game)</code>
        * [.logReward(user, label)](#module_game..Game+logReward) ⇒ <code>[Game](#Game)</code>
        * [.importSave()](#module_game..Game+importSave) ⇒ <code>[Game](#Game)</code>
        * [.fetchPlayer(user)](#module_game..Game+fetchPlayer) ⇒ <code>[Player](#Player)</code>
        * [.start()](#module_game..Game+start) ⇒ <code>[Game](#Game)</code>
    * [~handleEnter(entrance)](#module_game..handleEnter) ⇒ <code>undefined</code>
    * [~handleTip(tip)](#module_game..handleTip) ⇒ <code>undefined</code>

<a name="module_game..Game"></a>

### game~Game
**Kind**: inner class of <code>[game](#module_game)</code>  

* [~Game](#module_game..Game)
    * [.spawnEnemy()](#module_game..Game+spawnEnemy) ⇒ <code>[Game](#Game)</code>
    * [.setupPrizes()](#module_game..Game+setupPrizes) ⇒ <code>[Game](#Game)</code>
    * [.setupLevelUpRequirement()](#module_game..Game+setupLevelUpRequirement) ⇒ <code>[Game](#Game)</code>
    * [.setupRanks()](#module_game..Game+setupRanks) ⇒ <code>[Game](#Game)</code>
    * [.addPlayer(user, characterClass)](#module_game..Game+addPlayer) ⇒ <code>[Game](#Game)</code>
    * [.logReward(user, label)](#module_game..Game+logReward) ⇒ <code>[Game](#Game)</code>
    * [.importSave()](#module_game..Game+importSave) ⇒ <code>[Game](#Game)</code>
    * [.fetchPlayer(user)](#module_game..Game+fetchPlayer) ⇒ <code>[Player](#Player)</code>
    * [.start()](#module_game..Game+start) ⇒ <code>[Game](#Game)</code>

<a name="module_game..Game+spawnEnemy"></a>

#### game.spawnEnemy() ⇒ <code>[Game](#Game)</code>
Replaces current enemy with a newly generated one

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - (self)  
<a name="module_game..Game+setupPrizes"></a>

#### game.setupPrizes() ⇒ <code>[Game](#Game)</code>
Setup game prizes

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  
<a name="module_game..Game+setupLevelUpRequirement"></a>

#### game.setupLevelUpRequirement() ⇒ <code>[Game](#Game)</code>
Set the required amount of experience points players need to level up

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  
<a name="module_game..Game+setupRanks"></a>

#### game.setupRanks() ⇒ <code>[Game](#Game)</code>
Set the required amount of experience points players need to pass ranks

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  
<a name="module_game..Game+addPlayer"></a>

#### game.addPlayer(user, characterClass) ⇒ <code>[Game](#Game)</code>
Add a given player to the game, by name

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | The username of the player to add |
| characterClass | <code>string</code> | Name of the player's selected character class |

<a name="module_game..Game+logReward"></a>

#### game.logReward(user, label) ⇒ <code>[Game](#Game)</code>
Keeps track of critical hits and item usage

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - (self)  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the rewarded player |
| label | <code>string</code> | Description/name of the reward |

<a name="module_game..Game+importSave"></a>

#### game.importSave() ⇒ <code>[Game](#Game)</code>
Import the save from save input

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  
<a name="module_game..Game+fetchPlayer"></a>

#### game.fetchPlayer(user) ⇒ <code>[Player](#Player)</code>
Gets a player by username, adding to the game if they're not already playing

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Player](#Player)</code> - The given player  

| Param | Type | Description |
| --- | --- | --- |
| user | <code>string</code> | Username of the given user |

<a name="module_game..Game+start"></a>

#### game.start() ⇒ <code>[Game](#Game)</code>
Start the game, binding event handlers

**Kind**: instance method of <code>[Game](#module_game..Game)</code>  
**Returns**: <code>[Game](#Game)</code> - `Game` instance  
<a name="module_game..handleEnter"></a>

### game~handleEnter(entrance) ⇒ <code>undefined</code>
Handle users entering

**Kind**: inner method of <code>[game](#module_game)</code>  

| Param | Type | Description |
| --- | --- | --- |
| entrance | <code>string</code> | Chat entrance info |

<a name="module_game..handleTip"></a>

### game~handleTip(tip) ⇒ <code>undefined</code>
Handle chat tips

**Kind**: inner method of <code>[game](#module_game)</code>  
**this**: <code>[Game](#Game)</code>  

| Param | Type | Description |
| --- | --- | --- |
| tip | <code>Object</code> | Hashmap of tip properties |

<a name="module_graphics"></a>

## graphics
Library of game graphics references (simple emoji codes) and text fallbacks

**Requires**: <code>module:assert</code>  
**Ts-check**:   
<a name="module_graphics..GRAPHICS"></a>

### graphics~GRAPHICS : <code>Map.&lt;string, Object&gt;</code>
All known graphics for the game

**Kind**: inner constant of <code>[graphics](#module_graphics)</code>  
<a name="module_info"></a>

## info
Provides game state information and user help

**Requires**: <code>module:Player</code>, <code>module:moment/min/moment.min</code>  
**Ts=check**:   

* [info](#module_info)
    * [~Info](#module_info..Info)
        * [.showWelcome([player])](#module_info..Info+showWelcome) ⇒ <code>[Info](#Info)</code>
        * [.showPlayerInfo([player])](#module_info..Info+showPlayerInfo) ⇒ <code>[Info](#Info)</code>
        * [.showRewards()](#module_info..Info+showRewards) ⇒ <code>[Info](#Info)</code>
        * [.showClasses([player])](#module_info..Info+showClasses) ⇒ <code>[Info](#Info)</code>
        * [.showHelp([player])](#module_info..Info+showHelp) ⇒ <code>[Info](#Info)</code>

<a name="module_info..Info"></a>

### info~Info
**Kind**: inner class of <code>[info](#module_info)</code>  

* [~Info](#module_info..Info)
    * [.showWelcome([player])](#module_info..Info+showWelcome) ⇒ <code>[Info](#Info)</code>
    * [.showPlayerInfo([player])](#module_info..Info+showPlayerInfo) ⇒ <code>[Info](#Info)</code>
    * [.showRewards()](#module_info..Info+showRewards) ⇒ <code>[Info](#Info)</code>
    * [.showClasses([player])](#module_info..Info+showClasses) ⇒ <code>[Info](#Info)</code>
    * [.showHelp([player])](#module_info..Info+showHelp) ⇒ <code>[Info](#Info)</code>

<a name="module_info..Info+showWelcome"></a>

#### info.showWelcome([player]) ⇒ <code>[Info](#Info)</code>
Send a welcome message to inform users of the game

**Kind**: instance method of <code>[Info](#module_info..Info)</code>  
**Returns**: <code>[Info](#Info)</code> - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | <code>[Player](#Player)</code> | Game participant to message |

<a name="module_info..Info+showPlayerInfo"></a>

#### info.showPlayerInfo([player]) ⇒ <code>[Info](#Info)</code>
Show current player state

**Kind**: instance method of <code>[Info](#module_info..Info)</code>  
**Returns**: <code>[Info](#Info)</code> - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | <code>[Player](#Player)</code> | Game participant to message |

<a name="module_info..Info+showRewards"></a>

#### info.showRewards() ⇒ <code>[Info](#Info)</code>
Show prize winner and time information

**Kind**: instance method of <code>[Info](#module_info..Info)</code>  
**Returns**: <code>[Info](#Info)</code> - `Info` instance  
<a name="module_info..Info+showClasses"></a>

#### info.showClasses([player]) ⇒ <code>[Info](#Info)</code>
Show information about hero classes

**Kind**: instance method of <code>[Info](#module_info..Info)</code>  
**Returns**: <code>[Info](#Info)</code> - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | <code>[Player](#Player)</code> | Game participant to message |

<a name="module_info..Info+showHelp"></a>

#### info.showHelp([player]) ⇒ <code>[Info](#Info)</code>
Show the help menu to a given user

**Kind**: instance method of <code>[Info](#module_info..Info)</code>  
**Returns**: <code>[Info](#Info)</code> - `Info` instance  

| Param | Type | Description |
| --- | --- | --- |
| [player] | <code>[Player](#Player)</code> | Game participant to message |

<a name="module_messenger"></a>

## messenger
Sends chat messages and announcements


* [messenger](#module_messenger)
    * [~Messenger](#module_messenger..Messenger)
        * [.whisper(message, [username])](#module_messenger..Messenger+whisper) ⇒ <code>[Messenger](#Messenger)</code>
        * [.say(message, [username])](#module_messenger..Messenger+say) ⇒ <code>[Messenger](#Messenger)</code>
        * [.alert(message, [username])](#module_messenger..Messenger+alert) ⇒ <code>[Messenger](#Messenger)</code>
        * [.announce(message, [username])](#module_messenger..Messenger+announce) ⇒ <code>[Messenger](#Messenger)</code>
        * [.taunt(message, [username])](#module_messenger..Messenger+taunt) ⇒ <code>[Messenger](#Messenger)</code>
    * [~COLORS](#module_messenger..COLORS)

<a name="module_messenger..Messenger"></a>

### messenger~Messenger
**Kind**: inner class of <code>[messenger](#module_messenger)</code>  

* [~Messenger](#module_messenger..Messenger)
    * [.whisper(message, [username])](#module_messenger..Messenger+whisper) ⇒ <code>[Messenger](#Messenger)</code>
    * [.say(message, [username])](#module_messenger..Messenger+say) ⇒ <code>[Messenger](#Messenger)</code>
    * [.alert(message, [username])](#module_messenger..Messenger+alert) ⇒ <code>[Messenger](#Messenger)</code>
    * [.announce(message, [username])](#module_messenger..Messenger+announce) ⇒ <code>[Messenger](#Messenger)</code>
    * [.taunt(message, [username])](#module_messenger..Messenger+taunt) ⇒ <code>[Messenger](#Messenger)</code>

<a name="module_messenger..Messenger+whisper"></a>

#### messenger.whisper(message, [username]) ⇒ <code>[Messenger](#Messenger)</code>
Output basic chat message

**Kind**: instance method of <code>[Messenger](#module_messenger..Messenger)</code>  
**Returns**: <code>[Messenger](#Messenger)</code> - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Body text |
| [username] | <code>string</code> | Username of the message recipient |

<a name="module_messenger..Messenger+say"></a>

#### messenger.say(message, [username]) ⇒ <code>[Messenger](#Messenger)</code>
Output a more noticeable message

**Kind**: instance method of <code>[Messenger](#module_messenger..Messenger)</code>  
**Returns**: <code>[Messenger](#Messenger)</code> - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Header text |
| [username] | <code>string</code> | Username of the message recipient |

<a name="module_messenger..Messenger+alert"></a>

#### messenger.alert(message, [username]) ⇒ <code>[Messenger](#Messenger)</code>
Output a heavily-emphasized message to a specific

**Kind**: instance method of <code>[Messenger](#module_messenger..Messenger)</code>  
**Returns**: <code>[Messenger](#Messenger)</code> - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Header text |
| [username] | <code>string</code> | Username of the message recipient |

<a name="module_messenger..Messenger+announce"></a>

#### messenger.announce(message, [username]) ⇒ <code>[Messenger](#Messenger)</code>
Output a heavily-emphasized message to all

**Kind**: instance method of <code>[Messenger](#module_messenger..Messenger)</code>  
**Returns**: <code>[Messenger](#Messenger)</code> - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Header text |
| [username] | <code>string</code> | Username of the message recipient |

<a name="module_messenger..Messenger+taunt"></a>

#### messenger.taunt(message, [username]) ⇒ <code>[Messenger](#Messenger)</code>
...

**Kind**: instance method of <code>[Messenger](#module_messenger..Messenger)</code>  
**Returns**: <code>[Messenger](#Messenger)</code> - `Messenger` instance  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Header text |
| [username] | <code>string</code> | Username of the message recipient |

<a name="module_messenger..COLORS"></a>

### messenger~COLORS
Chat message colors

**Kind**: inner constant of <code>[messenger](#module_messenger)</code>  
<a name="module_player"></a>

## player
Game party member

**Requires**: <code>module:d20</code>  

* [player](#module_player)
    * [~Player](#module_player..Player)
        * _instance_
            * [.emoji](#module_player..Player+emoji)
            * [.level](#module_player..Player+level)
            * [.changeClass(selectedClass)](#module_player..Player+changeClass) ⇒ <code>Object</code>
            * [.addItems(items)](#module_player..Player+addItems)
            * [.useItem(item)](#module_player..Player+useItem) ⇒ <code>Object</code>
            * [.attack(enemy, damage)](#module_player..Player+attack) ⇒ <code>Object</code>
        * _static_
            * [.setupAbility(ability, effect)](#module_player..Player.setupAbility) ⇒ <code>undefined</code>
            * [.setLevelUpRequirement(xp)](#module_player..Player.setLevelUpRequirement) ⇒ <code>undefined</code>
            * [.setRanks(xp)](#module_player..Player.setRanks) ⇒ <code>undefined</code>

<a name="module_player..Player"></a>

### player~Player
**Kind**: inner class of <code>[player](#module_player)</code>  

* [~Player](#module_player..Player)
    * _instance_
        * [.emoji](#module_player..Player+emoji)
        * [.level](#module_player..Player+level)
        * [.changeClass(selectedClass)](#module_player..Player+changeClass) ⇒ <code>Object</code>
        * [.addItems(items)](#module_player..Player+addItems)
        * [.useItem(item)](#module_player..Player+useItem) ⇒ <code>Object</code>
        * [.attack(enemy, damage)](#module_player..Player+attack) ⇒ <code>Object</code>
    * _static_
        * [.setupAbility(ability, effect)](#module_player..Player.setupAbility) ⇒ <code>undefined</code>
        * [.setLevelUpRequirement(xp)](#module_player..Player.setLevelUpRequirement) ⇒ <code>undefined</code>
        * [.setRanks(xp)](#module_player..Player.setRanks) ⇒ <code>undefined</code>

<a name="module_player..Player+emoji"></a>

#### player.emoji
Renders an emoji/avatar for the player

**Kind**: instance property of <code>[Player](#module_player..Player)</code>  
**Read only**: true  
<a name="module_player..Player+level"></a>

#### player.level
Calculates player level based on current XP and the level up requirement

**Kind**: instance property of <code>[Player](#module_player..Player)</code>  
**Read only**: true  
<a name="module_player..Player+changeClass"></a>

#### player.changeClass(selectedClass) ⇒ <code>Object</code>
Change the character class of the player

**Kind**: instance method of <code>[Player](#module_player..Player)</code>  
**Returns**: <code>Object</code> - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| selectedClass | <code>string</code> | Selected character class |

<a name="module_player..Player+addItems"></a>

#### player.addItems(items)
Adds a set of items to player's inventory

**Kind**: instance method of <code>[Player](#module_player..Player)</code>  

| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array.&lt;string&gt;</code> | Set of items to add to inventory |

<a name="module_player..Player+useItem"></a>

#### player.useItem(item) ⇒ <code>Object</code>
Expend a given item, removing it from inventory

**Kind**: instance method of <code>[Player](#module_player..Player)</code>  
**Returns**: <code>Object</code> - `Player` instance  

| Param | Type | Description |
| --- | --- | --- |
| item | <code>string</code> | Identifier of the item to use |

<a name="module_player..Player+attack"></a>

#### player.attack(enemy, damage) ⇒ <code>Object</code>
Attack a given enemy

**Kind**: instance method of <code>[Player](#module_player..Player)</code>  
**Returns**: <code>Object</code> - Attack result  

| Param | Type | Description |
| --- | --- | --- |
| enemy | <code>[Enemy](#Enemy)</code> | The enemy to attack |
| damage | <code>number</code> | Damage to apply (token) |

<a name="module_player..Player.setupAbility"></a>

#### Player.setupAbility(ability, effect) ⇒ <code>undefined</code>
Assign custom special effects for a given ability

**Kind**: static method of <code>[Player](#module_player..Player)</code>  

| Param | Type | Description |
| --- | --- | --- |
| ability | <code>string</code> | Name of the ability to change |
| effect | <code>string</code> | Special effect to apply |

<a name="module_player..Player.setLevelUpRequirement"></a>

#### Player.setLevelUpRequirement(xp) ⇒ <code>undefined</code>
Set requisite amount of xp for leveling

**Kind**: static method of <code>[Player](#module_player..Player)</code>  

| Param | Type | Description |
| --- | --- | --- |
| xp | <code>number</code> | Required amount of xp |

<a name="module_player..Player.setRanks"></a>

#### Player.setRanks(xp) ⇒ <code>undefined</code>
Set requisite amount of xp for pass ranks

**Kind**: static method of <code>[Player](#module_player..Player)</code>  

| Param | Type | Description |
| --- | --- | --- |
| xp | <code>number</code> | Required amount of xp |

<a name="Enemy"></a>

## Enemy
**Kind**: global class  

* [Enemy](#Enemy)
    * [new Enemy(attributes)](#new_Enemy_new)
    * [.name](#Enemy+name) : <code>string</code>
    * [.graphic](#Enemy+graphic) : <code>string</code>
    * [.hp](#Enemy+hp) : <code>number</code>
    * [.maxHP](#Enemy+maxHP) : <code>number</code>
    * [.taunts](#Enemy+taunts) : <code>Array</code>

<a name="new_Enemy_new"></a>

### new Enemy(attributes)

| Param | Type | Description |
| --- | --- | --- |
| attributes | <code>Object</code> | Initial enemy properties |
| attributes.name | <code>string</code> | Name of the enemy |
| attributes.emoji | <code>string</code> | Icon/visual |
| attributes.hp | <code>number</code> | Total hitpoints |

<a name="Enemy+name"></a>

### enemy.name : <code>string</code>
**Kind**: instance property of <code>[Enemy](#Enemy)</code>  
<a name="Enemy+graphic"></a>

### enemy.graphic : <code>string</code>
**Kind**: instance property of <code>[Enemy](#Enemy)</code>  
<a name="Enemy+hp"></a>

### enemy.hp : <code>number</code>
**Kind**: instance property of <code>[Enemy](#Enemy)</code>  
<a name="Enemy+maxHP"></a>

### enemy.maxHP : <code>number</code>
**Kind**: instance property of <code>[Enemy](#Enemy)</code>  
<a name="Enemy+taunts"></a>

### enemy.taunts : <code>Array</code>
**Kind**: instance property of <code>[Enemy](#Enemy)</code>  
<a name="Game"></a>

## Game
**Kind**: global class  

* [Game](#Game)
    * [new Game(cb)](#new_Game_new)
    * [.cb](#Game+cb) : <code>Object</code>
    * [.enemy](#Game+enemy) : <code>[Enemy](#Enemy)</code> &#124; <code>null</code>
    * [.players](#Game+players) : <code>Map</code>
    * [.prizes](#Game+prizes) : <code>Object</code>
    * [.winners](#Game+winners) : <code>Array.&lt;Object&gt;</code>
    * [.command](#Game+command) : <code>Command</code>
    * [.graphics](#Game+graphics) : <code>Graphics</code>
    * [.messenger](#Game+messenger) : <code>[Messenger](#Messenger)</code>
    * [.info](#Game+info) : <code>[Info](#Info)</code>
    * [.totalDamage](#Game+totalDamage) : <code>number</code>
    * [.totalKills](#Game+totalKills) : <code>number</code>
    * [.rewardLog](#Game+rewardLog) : <code>Array</code>

<a name="new_Game_new"></a>

### new Game(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Game+cb"></a>

### game.cb : <code>Object</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+enemy"></a>

### game.enemy : <code>[Enemy](#Enemy)</code> &#124; <code>null</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+players"></a>

### game.players : <code>Map</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+prizes"></a>

### game.prizes : <code>Object</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+winners"></a>

### game.winners : <code>Array.&lt;Object&gt;</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+command"></a>

### game.command : <code>Command</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+graphics"></a>

### game.graphics : <code>Graphics</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+messenger"></a>

### game.messenger : <code>[Messenger](#Messenger)</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+info"></a>

### game.info : <code>[Info](#Info)</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+totalDamage"></a>

### game.totalDamage : <code>number</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+totalKills"></a>

### game.totalKills : <code>number</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Game+rewardLog"></a>

### game.rewardLog : <code>Array</code>
**Kind**: instance property of <code>[Game](#Game)</code>  
<a name="Info"></a>

## Info
**Kind**: global class  

* [Info](#Info)
    * [new Info(messenger, game, graphics)](#new_Info_new)
    * [.messenger](#Info+messenger) : <code>[Messenger](#Messenger)</code>
    * [.game](#Info+game) : <code>[Game](#Game)</code>
    * [.graphics](#Info+graphics) : <code>Graphics</code>

<a name="new_Info_new"></a>

### new Info(messenger, game, graphics)

| Param | Type | Description |
| --- | --- | --- |
| messenger | <code>[Messenger](#Messenger)</code> | Injected `Messenger` dependency |
| game | <code>[Game](#Game)</code> | Injected `Game` dependency |
| graphics | <code>Graphics</code> | Injected `Graphics` dependency |

<a name="Info+messenger"></a>

### info.messenger : <code>[Messenger](#Messenger)</code>
**Kind**: instance property of <code>[Info](#Info)</code>  
<a name="Info+game"></a>

### info.game : <code>[Game](#Game)</code>
**Kind**: instance property of <code>[Info](#Info)</code>  
<a name="Info+graphics"></a>

### info.graphics : <code>Graphics</code>
**Kind**: instance property of <code>[Info](#Info)</code>  
<a name="Messenger"></a>

## Messenger
**Kind**: global class  

* [Messenger](#Messenger)
    * [new Messenger(cb)](#new_Messenger_new)
    * [.cb](#Messenger+cb) : <code>Object</code>

<a name="new_Messenger_new"></a>

### new Messenger(cb)

| Param | Type | Description |
| --- | --- | --- |
| cb | <code>Object</code> | Chaturbate interface |

<a name="Messenger+cb"></a>

### messenger.cb : <code>Object</code>
**Kind**: instance property of <code>[Messenger](#Messenger)</code>  
<a name="Player"></a>

## Player
**Kind**: global class  

* [Player](#Player)
    * [new Player(name)](#new_Player_new)
    * [.name](#Player+name) : <code>string</code>
    * [.xp](#Player+xp) : <code>number</code>
    * [.inventory](#Player+inventory) : <code>Array.&lt;string&gt;</code>

<a name="new_Player_new"></a>

### new Player(name)

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Username of the player |

<a name="Player+name"></a>

### player.name : <code>string</code>
**Kind**: instance property of <code>[Player](#Player)</code>  
<a name="Player+xp"></a>

### player.xp : <code>number</code>
**Kind**: instance property of <code>[Player](#Player)</code>  
<a name="Player+inventory"></a>

### player.inventory : <code>Array.&lt;string&gt;</code>
**Kind**: instance property of <code>[Player](#Player)</code>  
