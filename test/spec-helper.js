/* eslint-env mocha */

'use strict';

require('babel-polyfill');

const chai  = require('chai');
const sinon = require('sinon');

require('sinon-as-promised');

chai.should();
chai.use(require('chai-as-promised'));
chai.use(require('sinon-chai'));

global.sandbox = sinon.sandbox.create();

afterEach(() => {
  global.sandbox.restore();
});
